# Good Food 

## Mise en contexte

Projet de 6 mois de première année de master.

Le projet Good Food est un service permettant d'effectuer des commandes auprès de restaurants ne disposant pas de service de livraison. (Style Uber Eats).

Le projet comporte une application web, une application mobile, une API ainsi qu'un websocket.

Stack Technique: React, React Native, NodeJS, MongoDB, Docker

J'ai eu en charge le développement intégral de l'application web et j'ai contribué au développement de l'API ainsi que du websocket.

Voici les différentes fonctionnalités l'application web:
- Connexion et inscription de nouveau restaurants (avec envoi de documents).
- Création de menus personnalisés (pour les restaurants). Les menus sont affichés dans l'application mobile pour les utilisateurs finaux.
- Suivi de commande en live.
- Gestion des coordonnées des restaurants.

Une fois inscrits, les restaurants et leurs menus sont répertoriés dans l'application mobile et ainsi les clients peuvent passer des commandes.

## Quelques captures d'écran 

### Le menu 
<img src="https://i.imgur.com/o1zDzwt.png" alt="menu" width=400>

### Ajout d'un item dans le menu
<img src="https://i.imgur.com/XuiIQcs.png" alt="ajout-item-1" width=250>
<img src="https://i.imgur.com/xnG2DHX.png" alt="ajout-item-2" width=200>

### Suivi des commandes
<img src="https://i.imgur.com/AwPVSzu.png" alt="suivi-commande" width=400>
