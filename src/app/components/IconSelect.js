import React, { useEffect, useRef, useState } from 'react'
import { Button, Dropdown, Overlay, Popover } from "react-bootstrap";


export const ICONS = [
  "🍕", "🌭", "🍔", "🍟",
  "🥯", "🥪", "🥩", "🍗",
  "🍖", "🥙", "🧆", "🌮",
  "🌯", "🫔", "🥗", "🥘",
  "🫕", "🍝", "🍜", "🍲",
  "🍛", "🍣", "🍱", "🥟",
  "🦪", "🍤", "🍙", "🍪",
  "🥡", "☕️", "🍩", "🍦",
  "🥤", "🧁", "🍰"
];


function IconSelect(props) {
  const [show, setShow] = useState(false);
  const target = useRef(null);
  const popover = useRef(null);
  const [selected, setSelected] = useState(props.value || ICONS[0]);

  const selectItem = (icon) => {
    setSelected(icon);
    setShow(false);
  }

  useEffect(() => props.onSelect(selected), [selected]);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        popover.current && !popover.current.contains(event.target) &&
        target.current && !target.current.contains(event.target)
      ) {
        setShow(false);
      }
    }

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [popover]);

  return (
    <>
      <Overlay placement="right" target={target.current} show={show} popperConfig={{
        modifiers: [
          {
            name: 'offset',
            options: {
              offset: [0, 20],
            },
          },
        ],
      }}>
        <Popover id="popover-positioned-top" className="rounded-extra shadow">
          <Popover.Content ref={popover} className="icon-wrapper">
            {ICONS.map((icon, index) => (
              <Button
                key={index}
                className="icon-button-list"
                onClick={() => selectItem(icon)}
              >
                {icon}
              </Button>
            ))}
          </Popover.Content>
        </Popover>
      </Overlay>
      <Button
        ref={target}
        onClick={() => setShow(!show)}
        className={`${props.className} icon-button`}
      >
        {selected}
      </Button>
    </>
  )
}

export default IconSelect
