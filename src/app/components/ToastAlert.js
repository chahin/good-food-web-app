import React from 'react'
import { Alert, Toast } from "react-bootstrap";
import { WarningRounded } from "@material-ui/icons";

function ToastAlert({ ...props }) {
  const isError = props.variant === "danger";

  return (
    <div
      style={{
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 100
      }}
    >
      <Toast className="border-0 rounded-extra m-4" {...props} delay={props.delay || 3000} autohide>
        <Alert variant={props.variant} className="border-0 m-0 rounded-extra d-flex align-items-center z-index-max">
          {isError && <WarningRounded className="mr-3" />}
          <span>
            {props.children}
          </span>
        </Alert>
      </Toast>
    </div>
  )
}

export default ToastAlert;
