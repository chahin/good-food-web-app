import { GetAppRounded, StarRounded } from "@material-ui/icons";
import api from "app/service";
import React, { useState } from 'react'
import { Badge, Button, Card, Col } from "react-bootstrap"
import ConfirmModal from "./ConfirmModal";
import { useTranslation } from 'react-i18next';

function OrderCard(props) {
  const { t } = useTranslation();
  const { order } = props;
  const [confirmModalState, setConfirmModalState] = useState(null);

  const changeOrderStatus = (orderId, status) => {
    updateOrder(orderId, { status });
  }

  const updateOrder = (orderId, params) => {
    api.updateOrder(
      orderId,
      params,
      {
        loading: (val) => null,
        error: (err) => console.log(err),
        success: (res) => console.log(res)
      }
    )
  }

  const refuseOrder = (orderId) => {
    setConfirmModalState({
      show: true,
      onSubmit: (reason) => {
        props.onRefuse();
        updateOrder(orderId, { status: "refused", reason });
      },
    });
  }

  const nextStatus = (orderId, nextStatus) => {
    if (props.onNextStatus)
      props.onNextStatus();
    changeOrderStatus(orderId, nextStatus)
  }

  return (<Col lg={6} className="p-2">
    <Card className="rounded-extra h-100">
      <Card.Body className="d-flex flex-column">
        <div className="d-flex justify-content-between align-items-start mb-3">
          <div className="d-flex flex-column">
            <h5 className="font-bold">{order.number}</h5>
            <span className="font-small text-gray mb-1">{order.user.name}</span>
            <div className="font-small text-gray text-wrap mw-75">{order.address.label}</div>
            {order.comment && <div className="font-small text-gray text-wrap mw-75">
              <hr className="w-50 mx-0" />
              <div className="font-semi-bold font-small mb-1">{t("orders.comment")}</div>
              "{order.comment}"
            </div>}
          </div>
          <div className="d-flex flex-column align-items-end">
            <div>{new Date(order.date).toLocaleDateString("fr-FR")}</div>
            {(order.status === "inDelivery" || order.status === "delivered") && <Badge className="mt-2" variant="primary">{order.status === "inDelivery" ? "In Delivery" : "Delivered"}</Badge>}
            {
              order.rating && (
                <div className="d-flex mt-2">
                  {Array.from({ length: order.rating }, (_, index) => index + 1).map(e => <StarRounded className="text-primary" key={e} />)}
                </div>
              )
            }
          </div>
        </div>
        <div className="order-wrapper p-4 mb-3 d-flex flex-column">
          {order.content.map(meal => (
            <div className="orders-container mb-3" key={meal.id}>
              <span style={{ gridColumnStart: 1 }}>
                {meal.number}x
              </span>

              <span className="font-semi-bold" style={{ gridColumnStart: 2 }}>
                {meal.name}
              </span>

              <span style={{ gridColumnStart: 3, justifySelf: "end" }}>
                {meal.price.toFixed(2)}€
              </span>

              {
                meal.content && meal.content.map(modifier => modifier.content).map(choice => (
                  choice.map((choice, i) => <React.Fragment key={i}>
                    <span className="font-small text-muted" style={{ gridColumnStart: 2 }}>{choice.name}</span>
                    {choice.price && <span className="font-small text-muted" style={{ gridColumnStart: 3, justifySelf: "end" }}>{choice.price.toFixed(2)}€</span>}
                  </React.Fragment>)
                ))}
            </div>
          ))}

          <hr className="w-100 mt-auto" />
          <div className="d-flex justify-content-between">
            <span className="font-semi-bold">Total</span>
            <span className="font-semi-bold">
              {order.price.toFixed(2)}€
            </span>
          </div>

        </div>
        <div className="d-flex">
          {props.nextStatus !== "inPreparation" && props.nextStatus !== "refused" && (
            <a href={`${order.invoice}`} target="_blank">
              <Button><GetAppRounded /> {t("orders.invoice")}</Button>
            </a>
          )}
          <div className="ml-auto">
            {props.nextStatus === "inPreparation" && <Button className="mr-2" onClick={() => refuseOrder(order._id)}>{t("orders.refuse")}</Button>}
            {props.buttonLabel && <Button onClick={() => nextStatus(order._id, props.nextStatus)}>{props.buttonLabel}</Button>}
          </div>
        </div>
      </Card.Body>
    </Card>
    <ConfirmModal
      withInput
      show={confirmModalState?.show}
      onSubmit={confirmModalState?.onSubmit}
      onHide={() => setConfirmModalState(null)}
      placeholder={t("orders.reason")}
      title={`${t("orders.refuseOrder")} ${props.order.number}`}
      message={t("orders.clarifyReason")}
    />
  </Col>
  )
}

export default OrderCard
