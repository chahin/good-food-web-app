import React from 'react'
import { Button, Spinner } from "react-bootstrap"

function LoadingButton({ spinnerVariant = null, loading = false, ...rest }) {
  return (
    <Button {...rest}>
      <span>{rest.children}</span>
      {
        loading && (
          <Spinner
            className="ml-2"
            animation="border"
            size="sm"
            variant={spinnerVariant ? spinnerVariant : rest.variant}
          />
        )
      }
    </Button>
  )
}

export default LoadingButton
