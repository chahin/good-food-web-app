import React, { useEffect, useState } from 'react'
import { Button, Form, Modal } from "react-bootstrap";
import { WarningRounded } from "@material-ui/icons";
import { useTranslation } from 'react-i18next';

function ConfirmModal(props) {
  const { t } = useTranslation();
  const [inputText, setInputText] = useState("");
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
    return () => setMounted(false);
  }, []);

  const handleSubmit = () => {
    props.onHide();
    inputText.length > 0 ? props.onSubmit(inputText) : props.onSubmit();
    if (mounted)
      setTimeout(() => setInputText(""), 200);
  }

  return (
    <Modal backdrop="static" size="sm" show={props.show} onHide={props.onHide} centered>
      <Modal.Body>
        <div className="mb-3">
          <WarningRounded className="mb-2" />
          <div className="font-semi-bold mb-1 d-flex align-items-end">
            {props.title || t("areYouSure")}
          </div>
          <div className="font-small text-gray">{props.message || t("changesMayNotBeSaved")}</div>
        </div>
        {
          props.withInput && <Form.Control
            className="mb-3"
            type="text"
            name="input"
            placeholder={props.placeholder || "Message"}
            value={inputText}
            onChange={(e) => setInputText(e.target.value)}
          />
        }
        <div className="d-flex justify-content-end">
          <Button variant="dark" onClick={props.onHide}>
            {props.cancelText ?? t("cancel")}
          </Button>
          <Button variant="primary" className="ml-2" onClick={handleSubmit}>
            {props.submitText ?? t("yes")}
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default ConfirmModal
