import ls from 'localstorage-slim';
ls.config.encrypt = true;

export default {
  _token: null,
  _restaurant: null,
  _isAdmin: false,

  isLogged() {
    return this.token;
  },

  set token(str) {
    this._token = str;
    ls.set("good-food-token", str);
  },
  get token() {
    return this._token || ls.get("good-food-token");
  },

  set restaurant(object) {
    this._restaurant = object;
    ls.set("restaurant", JSON.stringify(object));
  },
  get restaurant() {
    return this._restaurant || JSON.parse(ls.get("restaurant"));
  },

  set isAdmin(bool) {
    this._isAdmin = bool;
    ls.set("is-admin", bool);
  },
  get isAdmin() {
    return this._isAdmin || ls.get("is-admin");
  },

  clear() {
    ls.clear();
    this._token = null;
    this._restaurant = null;
    this._isAdmin = false;
  }
}