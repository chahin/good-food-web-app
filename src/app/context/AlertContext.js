// @ts-nocheck
import React, { useState, createContext, useEffect } from 'react';
import { useHistory, useLocation } from "react-router";
import ToastAlert from "../components/ToastAlert";

const AlertContext = createContext(({ message, type = "success", delay = 3000 }) => { });

const AlertProvider = (props) => {
  const location = useLocation();
  const history = useHistory();
  const [show, setShow] = useState(false);
  const [message, setMessage] = useState(null);
  const [type, setType] = useState("success");
  const [delay, setDelay] = useState(3000);
  const [timeoutId, setTimeoutId] = useState(null);

  useEffect(() => {
    if (location.state?.alert) {
      const { message, type } = location.state.alert;
      showAlert(message, type);
      const state = { ...location.state };
      delete state.alert;
      history.replace({ ...history.location, state });
    }
  }, [location]);

  const resetState = () => {
    setMessage(null);
    setType("success");
    setDelay(3000);
  }

  const showAlert = ({ message, type = "success", delay = 3000 }) => {
    if (timeoutId)
      clearTimeout(timeoutId);
    if (type === "error")
      setType("danger");
    else if (type === "info")
      setType("warning");
    else
      setType("success");
    setMessage(message);
    setDelay(delay);
    setShow(true);
  }

  const hideAlert = () => {
    setShow(false);
    setTimeoutId(setTimeout(resetState, 300));
  }

  return (
    <AlertContext.Provider value={showAlert}>
      <ToastAlert show={show} onClose={hideAlert} variant={type} delay={delay}>
        {message}
      </ToastAlert>

      {props.children}
    </AlertContext.Provider>
  );
}

export { AlertContext, AlertProvider };