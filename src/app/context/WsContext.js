import api from "app/service";
import store from "app/store";
import React, { useState, createContext, useContext } from 'react';
import { AlertContext } from ".";
const WsContext = createContext({ closeWs: () => { }, initializeWs: () => { }, newOrders: [], newOrdersLoading: false, remove: (orderId) => { } });

let waitForWebsocketsConnexion;
let ws;
const WsProvider = (props) => {
  const [newOrders, setNewOrders] = useState([]);
  const [loading, setLoading] = useState(false);
  const showAlert = useContext(AlertContext);

  const wsConnection = () => {
    ws = new WebSocket("ws://good-food.fr:4000")
    ws.onerror = () => wsReconnection()

    ws.onopen = (e) => {
      console.log("CONNECTED");
      ws.send(JSON.stringify({ isRestaurant: true, _id: store.restaurant._id }));
    }

    ws.onmessage = (e) => {
      api.getOrders(
        { status: "waiting" },
        {
          loading: (val) => setLoading(val),
          error: (err) => console.log(err),
          success: (res) => {
            showAlert({ message: "You have a new order!" })
            setNewOrders(res);
          }
        }
      )
    };
  }

  const closeWs = () => {
    if (ws)
      ws.close();
  };

  const wsReconnection = () => {
    clearInterval(waitForWebsocketsConnexion)
    waitForWebsocketsConnexion = setInterval(() => wsConnection(), 1000);
  }

  const remove = (orderId) => {
    setNewOrders(newOrders.filter(o => o._id !== orderId));
  }

  return (
    <WsContext.Provider value={{ closeWs, initializeWs: wsConnection, newOrders, newOrdersLoading: loading, remove }}>
      {props.children}
    </WsContext.Provider>
  );
}

export { WsContext, WsProvider };