// @ts-nocheck
import store from "app/store";
import axios from "axios";
import { history } from "../../router";
import fileType from "file-type";

const http = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: { "Content-Type": "application/json" },
});

const request = (
  { payload, loading, success, error, headers = {}, docName, params, ...rest }, method
) => {
  loading(true);

  if (params) {
    for (const key in params) {
      if (Array.isArray(params[key]))
        params[key] = params[key].join(",");
    }
  }

  let config = { method, headers: { ...headers }, params, ...rest };


  if (store.token)
    config.headers.Authorization = `Bearer ${store.token}`;

  if (method !== "GET")
    config.data = payload;

  return http(config)
    .then(async res => {
      let result = res.data;
      if (docName) {
        const buffer = await result.arrayBuffer();
        const { ext } = await fileType.fromBuffer(buffer);
        result = { file: result, filename: `${docName}.${ext}` };
      }
      success(result);
    })
    .then(() => loading(false))
    .catch(err => {
      if (err.response) {
        const { status, config: { url } } = err.response;
        if (!url.includes('signin') && !url.includes('signup') && (status === 401 || status === 403)) {
          store.clear();
          history.push('/signin', {
            alert: {
              type: "error",
              message: "You must log in"
            }
          });
        } else {
          error(err);
        }
      } else
        error(error);
      loading(false);
    });
}

export const httpGet = (params) => {
  request(params, "GET");
}

export const httpPost = (params) => {
  request(params, "POST");
}

export const httpPatch = (params) => {
  request(params, "PATCH");
}

export const httpDelete = (params) => {
  request(params, "DELETE");
}

export const httpPostDocuments = (params) => {
  request({
    ...params,
    headers: {
      "Content-Type": "multipart/form-data",
      ...params.headers
    }
  },
    "POST"
  );
}

export const downloadDocument = (params) => {
  request({ ...params, responseType: "blob" }, "GET");
}

export default http;

