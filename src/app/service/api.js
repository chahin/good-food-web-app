import { downloadDocument, httpGet, httpPost, httpPatch, httpPostDocuments, httpDelete } from "./http"
const BASE_URL = "/restaurants"

export default {
  // restaurants
  signUp: (restaurant, mutators) => httpPost({ url: "/signup-restaurant", payload: restaurant, ...mutators }),
  signIn: (restaurant, mutators) => httpPost({ url: "/signin-restaurant", payload: restaurant, ...mutators }),
  whoAmI: (mutators) => httpGet({ url: "/restaurants/whoami", ...mutators }),
  updateRestaurant: (restaurant, mutators) => httpPatch({ url: BASE_URL, payload: restaurant, ...mutators }),
  uploadDocuments: (restaurant, documents, mutators) => httpPostDocuments(
    {
      url: `${BASE_URL}/upload-documents`,
      payload: documents,
      headers: { credentials: JSON.stringify(restaurant) },
      ...mutators
    },
  ),
  uploadImages: (images, mutators) => httpPostDocuments({ url: `${BASE_URL}/upload-images`, payload: images, ...mutators }),
  uploadMealImage: (menuId, mealImage, mutators) => httpPostDocuments(
    {
      url: `${BASE_URL}/upload-meal-image`,
      payload: mealImage,
      headers: { "menu-id": JSON.stringify(menuId) },
      ...mutators
    },
  ),
  deleteMealImage: (menuId, mutators) => httpDelete({ url: `${BASE_URL}/delete-meal-image`, payload: menuId, ...mutators }),
  getImages: (mutators) => httpGet({ url: `${BASE_URL}/images`, ...mutators }),
  getOrders: (params, mutators) => httpGet({ url: `${BASE_URL}/orders`, params, ...mutators }),
  updateOrder: (orderId, params, mutators) => httpPatch({ url: `/orders/${orderId}`, payload: params, ...mutators }),


  // Admin
  signInAdmin: (admin, mutators) => httpPost({ url: "/signin-admin", payload: admin, ...mutators }),
  getAllUnverified: (mutators) => httpGet({ url: `${BASE_URL}?verified=false&pending=true`, ...mutators }),
  downloadId: (restaurantId, mutators) => downloadDocument({ url: `${BASE_URL}/${restaurantId}/download-id`, docName: "id", ...mutators },),
  downloadSiret: (restaurantId, mutators) => downloadDocument({ url: `${BASE_URL}/${restaurantId}/download-siret`, docName: "siret", ...mutators }),
  approveRestaurant: (restaurantId, mutators) => httpPost({ url: `${BASE_URL}/${restaurantId}/approve`, ...mutators }),
  rejectRestaurant: (restaurantId, mutators) => httpPost({ url: `${BASE_URL}/${restaurantId}/reject`, ...mutators }),

  // // Users
  // users: () => http.get("/users"),
  // user: (id) => http.get(`/users/${id}`),
  // friendsOfUser: (id) => http.get(`/users/${id}/friends`),

  // // logged user
  // myFriends: () => http.get(`/users/${store.id}/friends`),
  // addFriend: (id) => http.put(`/users/${store.id}/friends/${id}`),
  // removeFriend: (id) => http.delete(`/users/${store.id}/friends/${id}`),

  // // invites
  // myInvites: () => http.get(`/users/${store.id}/invites`),
  // invitesOfUser: (userId) => http.get(`/users/${userId}/invites`),
  // inviteUserToGame: (userId, gameId) => http.put(`/users/${userId}/invites/${gameId}`),
  // rmUserInviteOfGame: (userId, gameId) => http.put(`/users/${userId}/invites/${gameId}`),

  // // games
  // games: () => http.get("/partie"),
  // createGame: (game) => http.post("/partie", game),
  // editGame: (id, game) => http.put(`/partie/${id}`, game),
  // deleteGame: (id) => http.delete(`/partie/${id}`),
  // gamesGuests: (id) => http.get(`/partie/${id}/invites`),
  // gamePlayers: (id) => http.get(`/partie/${id}/players`),
  // addUserToGame: (gameId, userId) => http.put(`/partie/${gameId}/players/${userId}`),
  // removeUserFromGame: (gameId, userId) => http.delete(`/partie/${gameId}/players/${userId}`),
};