import React from 'react'

function NotFound() {
  return (
    <div className="vh-100 d-flex justify-content-center align-items-center">
      <div className="text-center">
        <h3 className="font-bold">404</h3>
        <h3 className="text-gray">Not found</h3>
      </div>
    </div>
  )
}

export default NotFound
