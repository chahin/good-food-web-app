import React, { useRef, useEffect, useState, useMemo, useContext } from "react"; import { Button, Container, Row, Col, Form, Alert, Carousel, Toast } from "react-bootstrap"; import { Link, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import * as yup from "yup";
import api from "../../service/api";
import mapboxgl from "mapbox-gl";
import MapboxGeocoder from "@mapbox/mapbox-gl-geocoder";
import "@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css";
import "mapbox-gl/dist/mapbox-gl.css";
import { ArrowBackRounded, ArrowForwardRounded, NoteAddRounded } from "@material-ui/icons";
import { AlertContext } from "app/context/AlertContext";
import goodFoodLogo from "../../../assets/good-food.svg";
import { useTranslation } from 'react-i18next';

mapboxgl.accessToken = "pk.eyJ1IjoiY2hhaGluIiwiYSI6ImNrdGhkbXJ5NzBnOWYyd282Z2wxZzQ0dGIifQ.EAeGBRKqhIsWK8v2kGKrIw";

const TOTAL_STEP = 2;
const INITIAL_ZOOM = 3.5;
const INITIAL_CENTER = [2.373867, 47.062874];
const CATEGORIES = [
  "African",
  "Asian",
  "Chinese",
  "Fast-Food",
  "Indian",
  "Italian",
  "Japanese",
  "Korean",
  "Turkish"
];


const HourSelect = ({ min = 0, max = 23, onChange, ...rest }) => {
  let hours = [];

  for (var i = min; i <= max; i++)
    hours.push(i);

  return (<Form.Control as="select" size="sm" {...rest} onChange={(e) => onChange(Number(e.target.value))}>
    {hours.map(c => <option key={c} value={c} >{c} h</option>)}
  </Form.Control>)
}


const SignUp = () => {
  const { t } = useTranslation();
  const [isError, setIsError] = useState(false);
  const history = useHistory();
  const [currentStep, setCurrentStep] = useState(0);
  const [isTwoShifts, setIsTwoShifts] = useState(false);
  const [form1Data, setForm1Data] = useState({
    email: "",
    password: "",
    confirmPassword: "",
    name: "",
    category: "",
    iban: "",
    bic: "",
    idFile: null,
    siretFile: null
  });
  const [validated, setValidated] = useState(false);
  const [image] = useState(new Image().src = goodFoodLogo)
  const idFileInput = useRef(null);
  const siretFileInput = useRef(null);

  const showAlert = useContext(AlertContext);

  const [hours, setHours] = useState({
    open1: 11,
    close1: 14,
    open2: 19,
    close2: 22
  });

  const [hourErrors, setHourErrors] = useState({});
  const [address, setAddress] = useState(null);
  const isFirstStep = useMemo(() => currentStep === 0, [currentStep]);
  const isLastStep = useMemo(() => currentStep === TOTAL_STEP - 1, [currentStep]);
  const addressIsInvalid = useMemo(() => validated && !address, [address, validated]);
  const form2IsValid = useMemo(() => validated && Object.keys(hourErrors).length === 0 && !!address, [validated, address, hourErrors])

  // map
  const mapContainer = useRef(null);
  const map = useRef(null);
  const geocoderRef = useRef(null);
  const geocoderInput = useRef(null)

  let marker;

  useEffect(() => {
    if (geocoderInput.current) {
      if (addressIsInvalid)
        geocoderInput.current.classList.add("is-invalid");
      else
        geocoderInput.current.classList.remove("is-invalid");
    }
  }, [addressIsInvalid]);

  const resetMap = () => {
    setAddress(null);
    if (marker)
      marker.remove();
    map.current.setZoom(INITIAL_ZOOM);
    map.current.setCenter(INITIAL_CENTER);
  }

  useEffect(() => {
    if (!map.current) { // initialize map only once
      map.current = new mapboxgl.Map({
        container: mapContainer.current,
        style: "mapbox://styles/mapbox/streets-v11",
        // @ts-ignore
        center: INITIAL_CENTER,
        zoom: INITIAL_ZOOM,
        minZoom: 3.5,
        maxZoom: 17.5,
        attributionControl: false
      });

      map.current.on("load", () => map.current.resize());
      map.current.dragRotate.disable();

      const geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: map.current,
        marker: false
      });

      geocoder.on("result", e => {
        setAddress({ label: e.result.place_name, coordinates: e.result.center });
        geocoderInput.current.blur();
        if (marker)
          marker.remove();
        marker = new mapboxgl.Marker({ color: "orange" })
          .setLngLat(e.result.center)
          .addTo(map.current);
      });

      geocoderRef.current.appendChild(geocoder.onAdd(map.current));

      geocoderInput.current = geocoderRef.current.querySelector(".mapboxgl-ctrl-geocoder--input");
      geocoderInput.current.classList.add("form-control");
      geocoderInput.current.addEventListener("input", resetMap);
      geocoderInput.current.setAttribute("autocomplete", "false");

      const geocoderClearButton = geocoderRef.current.querySelector(".mapboxgl-ctrl-geocoder--button");
      geocoderClearButton.addEventListener("click", resetMap);
    }
  });

  useEffect(() => {
    map.current.resize();

  }, []);

  useEffect(() => {
    map.current.resize();
  }, [currentStep, isTwoShifts, validated]);

  useEffect(() => {
    let errors = {};

    if (hours.open1 === hours.close1)
      errors = { ...errors, open1: true, close1: true };

    if (isTwoShifts) {
      if (hours.open1 > hours.close1) {
        if (!(hours.open2 > hours.close1 && hours.open2 < hours.open1))
          errors = { ...errors, open2: true };

        if (hours.close2 >= hours.open1)
          errors = { ...errors, close2: true };
      } else if (hours.open1 < hours.close1) {

        if (hours.open2 <= hours.close1)
          errors = { ...errors, open2: true };

        if (hours.close2 <= hours.open2)
          errors = { ...errors, close2: true };
      }
    }

    setHourErrors({ ...errors })
  }, [hours, isTwoShifts, validated]);

  useEffect(() => {
    if (form2IsValid) {
      form2HandleSubmit();
    }
  }, [validated]);

  const form2HandleSubmit = () => {
    if (form2IsValid) {
      signUp({
        ...form1Data,
        address,
        hours: isTwoShifts ? [[hours.open1, hours.close1], [hours.open2, hours.close2]] : [[hours.open1, hours.close1]]
      });
    } else
      setValidated(true);
  };

  const uploadDocuments = () => {
    const documents = new FormData();
    documents.append("id", form1Data.idFile);
    documents.append("siret", form1Data.siretFile);

    const credentials = {
      email: form1Data.email,
      password: form1Data.password
    }

    api.uploadDocuments(
      credentials,
      documents,
      {
        loading: (val) => null,
        error: (err) => showAlert({ message: "An error occured. Could not sign up", type: "error" }),
        success: (res) => {
          showAlert({ message: "Application successfully sent. An email has been sent.", delay: 10000 })
          history.replace("/signin");
        }
      }
    )
  }

  const signUp = (signUpPayload) => {
    api.signUp(
      signUpPayload,
      {
        loading: (val) => null,
        error: () => showAlert({ message: "An error occured. Could not sign up", type: "error" }),
        success: (res) => uploadDocuments()
      });
  }

  const form1 = useFormik({
    validationSchema: yup.object().shape({
      email: yup.string().email("Invalid email").required(t("signUp.required")),
      password: yup.string().required(t("signUp.required")),
      confirmPassword: yup.string()
        .oneOf([yup.ref("password"), null], "Passwords must match")
        .required(t("signUp.required")),
      name: yup.string()
        .min(2)
        .max(50)
        .required(t("signUp.required")),
      // category: yup.string().oneOf(CATEGORIES).required(t("signUp.required")),
      iban: yup.string().required(t("signUp.required")).min(14, "Too short").max(34, "Too long"),
      bic: yup.string().required(t("signUp.required")).min(8, "Too short").max(11, "Too long"),
      idFile: yup.mixed().required(t("signUp.required")),
      siretFile: yup.mixed().required(t("signUp.required"))
    }),
    initialValues: form1Data,
    onSubmit: (values, { setSubmitting, resetForm }) => {
      setCurrentStep(currentStep + 1);
      const { confirmPassword, ...rest } = values;
      setForm1Data({ ...form1Data, ...rest });
    }
  });

  return <Container fluid className="d-flex vh-100 bg-black overflow-auto p-0 align-items-stretch">
    <Col md={5} className="bg-danger">

    </Col>
    <Col md={7} className="p-0">
      <div className="bg-white rounded-extra h-100 d-flex flex-column align-items-center py-5">
        <Col md={10} sm={12} lg={8} className="flex-basis-0 d-flex align-items-start justify-content-between">
          <div>
            <h3 className="font-semi-bold">{t("signUp.title")}</h3>
            <p className="text-light-gray font-small">{t("signUp.step")} {currentStep + 1} / {TOTAL_STEP}</p>
          </div>
          <img src={image} style={{ height: "4em" }} />
        </Col>
        <Form className="d-flex flex-column flex-grow-1 justify-content-center w-100">
          <Carousel
            className="flex-grow-1"
            activeIndex={currentStep}
            // onSelect={handleSelect}
            controls={false}
            indicators={false}
            interval={null}
          >
            <Carousel.Item>
              <div className="px-5 h-100 w-100 d-flex flex-column justify-content-center align-items-center">
                <Col md={10} sm={12} lg={7} className="flex-basis-0">
                  <Form.Row>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.email")}</Form.Label>
                      <Form.Control
                        type="text"
                        name="email"
                        value={form1.values.email}
                        onChange={form1.handleChange}
                        isInvalid={form1.touched.email && !!form1.errors.email}
                      />
                      <Form.Control.Feedback type="invalid">
                        {form1.errors.email}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.password")}</Form.Label>
                      <Form.Control
                        type="password"
                        name="password"
                        value={form1.values.password}
                        onChange={form1.handleChange}
                        isInvalid={form1.touched.password && !!form1.errors.password}
                      />
                      <Form.Control.Feedback type="invalid">
                        {form1.errors.password}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.confirmPassword")}</Form.Label>
                      <Form.Control
                        type="password"
                        name="confirmPassword"
                        value={form1.values.confirmPassword}
                        onChange={form1.handleChange}
                        isInvalid={
                          form1.touched.confirmPassword && !!form1.errors.confirmPassword
                        }
                      />
                      <Form.Control.Feedback type="invalid">
                        {form1.errors.confirmPassword}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.restaurantName")}</Form.Label>
                      <Form.Control
                        type="text"
                        name="name"
                        value={form1.values.name}
                        onChange={form1.handleChange}
                        isInvalid={form1.touched.name && !!form1.errors.name}
                      />
                      <Form.Control.Feedback type="invalid">
                        {form1.errors.name}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.category")}</Form.Label>
                      <Form.Control
                        as="select"
                        size="sm"
                        name="category"
                        isInvalid={form1.touched.category && !!form1.errors.category}
                        onChange={form1.handleChange}
                      >
                        <option value="" className="text-gray">{t("signUp.choose")}</option>
                        <option disabled>----</option>
                        {CATEGORIES.map(c => t(`categories.${c}`)).sort().map((c, i) => <option key={i} value={c}>{c}</option>)}
                      </Form.Control>
                      <Form.Control.Feedback type="invalid">
                        {form1.errors.category}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.iban")}</Form.Label>
                      <Form.Control
                        type="text"
                        name="iban"
                        className="capitalize"
                        value={form1.values.iban}
                        onChange={form1.handleChange}
                        isInvalid={form1.touched.iban && !!form1.errors.iban}
                      />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.bic")}</Form.Label>
                      <Form.Control
                        type="text"
                        name="bic"
                        className="capitalize"
                        value={form1.values.bic}
                        onChange={form1.handleChange}
                        isInvalid={form1.touched.bic && !!form1.errors.bic}
                      />
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.importantDocs")}</Form.Label>
                      <Form.Row>
                        <Col>
                          <Button
                            variant={form1.touched.idFile && form1.errors.idFile ? "outline-danger" : "secondary"}
                            className="d-flex w-100 justify-content-center align-items-center"
                            onClick={() => idFileInput.current.click()}
                          >
                            <NoteAddRounded />
                            <span className="ml-2">{t("signUp.importId")}</span>
                          </Button>
                          {form1.touched.idFile && form1.errors.idFile && <span className="text-danger font-small mt-1">
                            {form1.errors.idFile}
                          </span>}
                          <div className="text-gray font-small mt-2 text-center">
                            {form1.values.idFile && form1.values.idFile.name}
                          </div>
                        </Col>
                        <Col>
                          <Button
                            variant={form1.touched.siretFile && form1.errors.siretFile ? "outline-danger" : "secondary"}
                            className="d-flex w-100 justify-content-center align-items-center"
                            onClick={() => siretFileInput.current.click()}
                          >
                            <NoteAddRounded />
                            <span className="ml-2">{t("signUp.importSiret")}</span>
                          </Button>
                          {form1.touched.siretFile && form1.errors.siretFile && <span className="text-danger font-small mt-1">
                            {form1.errors.siretFile}
                          </span>}
                          <div className="text-gray font-small mt-2 text-center">
                            {form1.values.siretFile && form1.values.siretFile.name}
                          </div>
                        </Col>
                      </Form.Row>
                      <Form.Control
                        ref={idFileInput}
                        type="file"
                        name="idFile"
                        accept="image/png, image/jpeg, .pdf"
                        // @ts-ignore
                        onChange={(e) => form1.setValues({ ...form1.values, idFile: e.target.files[0] })}
                      />

                      <Form.Control
                        ref={siretFileInput}
                        type="file"
                        name="siretFile"
                        accept="image/png, image/jpeg, .pdf"
                        // @ts-ignore
                        onChange={(e) => form1.setValues({ ...form1.values, siretFile: e.target.files[0] })}
                      />
                    </Form.Group>
                  </Form.Row>
                </Col>
              </div>
            </Carousel.Item>
            <Carousel.Item>
              <div className="px-5 w-100 h-100 py-5 d-flex justify-content-center align-items-stretch">
                <Col md={10} sm={12} lg={8} className="d-flex flex-column">
                  <Form.Row>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.hours")}</Form.Label>
                      <Form.Check
                        id="isTwoShifts"
                        name="isTwoShifts"
                        type="switch"
                        checked={isTwoShifts}
                        className="mb-1"
                        label="2 shifts"
                        onChange={(e) => setIsTwoShifts(e.target.checked)}
                      />
                      <Form.Group as={Form.Row} className="my-0">
                        <Col md={5}>
                          <HourSelect
                            name="hoursOpen1"
                            value={hours.open1}
                            onChange={(h) => setHours({ ...hours, open1: h })}
                            // @ts-ignore
                            isInvalid={validated && !!hourErrors.open1}
                          />
                        </Col>
                        <Col md={2} className="d-flex justify-content-center">
                          <ArrowForwardRounded />
                        </Col>
                        <Col md={5}>
                          <HourSelect
                            name="hoursClose1"
                            value={hours.close1}
                            onChange={(h) => setHours({ ...hours, close1: h })}
                            // @ts-ignore
                            isInvalid={validated && !!hourErrors.close1}
                          />
                        </Col>
                      </Form.Group>
                      {isTwoShifts && <Form.Group as={Form.Row} className="mb-0 mt-2">
                        <Col md={5}>
                          <HourSelect
                            name="hoursOpen2"
                            value={hours.open2}
                            onChange={(h) => setHours({ ...hours, open2: h })}
                            // @ts-ignore
                            isInvalid={validated && !!hourErrors.open2}
                          />
                        </Col>
                        <Col md={2} className="d-flex justify-content-center">
                          <ArrowForwardRounded />
                        </Col>
                        <Col md={5}>
                          <HourSelect
                            name="hoursClose2"
                            value={hours.close2}
                            onChange={(h) => setHours({ ...hours, close2: h })}
                            // @ts-ignore
                            isInvalid={validated && !!hourErrors.close2}
                          />
                        </Col>
                      </Form.Group>}
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col}>
                      <Form.Label>{t("signUp.address")}</Form.Label>
                      <div ref={geocoderRef} className="geocoder w-100" />
                      {addressIsInvalid && <span className="text-danger font-small mt-1">
                        {t("signUp.wrongAddress")}
                      </span>}
                    </Form.Group>
                  </Form.Row>
                  <div ref={mapContainer} className="map-container" />
                </Col>
              </div>
            </Carousel.Item>
          </Carousel>
        </Form>
        <Col md={10} sm={12} lg={8} className="flex-basis-0 d-flex">

          <div className="ml-auto d-flex flex-column align-items-end">
            <div className="d-flex mb-2">
              {
                isLastStep && <Button
                  disabled={isFirstStep}
                  className="d-flex ml-auto mr-2"
                  onClick={() => setCurrentStep(currentStep - 1)}
                  variant="primary"
                >
                  <div className="d-flex align-items-center">
                    <ArrowBackRounded className="mr-2" />
                    <div>{t("signUp.previous")}</div>
                  </div>
                </Button>
              }
              <Button
                onClick={() => { isLastStep ? form2HandleSubmit() : form1.handleSubmit() }}
                // onClick={() => form2HandleSubmit()}
                variant="primary"
              >
                {
                  isLastStep
                    ? t("signUp.submit")
                    : (
                      <div className="d-flex align-items-center">
                        <div>{t("signUp.next")}</div>
                        <ArrowForwardRounded className="ml-2" />
                      </div>
                    )
                }
              </Button>
            </div>
            <Link to="/signin" className="text-light-gray font-small">
              {t("signUp.alreadyAccount")}
            </Link>
          </div>
        </Col>
      </div>
    </Col >
  </Container>;
};

export default SignUp;