import React, { useContext, useEffect, useState } from "react";
import { Button, Col, Container, Form } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import * as yup from "yup";
import api from "app/service";
import store from "app/store";
import { AlertContext } from "app/context/AlertContext";
import goodFoodLogo from "../../../assets/good-food.svg";
import { useTranslation } from 'react-i18next';
import { WsContext } from "app/context";

const SignUp = function () {
  const { t } = useTranslation();
  const history = useHistory();
  const showAlert = useContext(AlertContext);
  const { initializeWs, closeWs } = useContext(WsContext);
  const [image] = useState(new Image().src = goodFoodLogo);

  useEffect(() => closeWs(), []);

  const signInError = () => showAlert({ message: "Wrong login or password", type: "error" });

  const signInAdmin = (signInPayload) => {
    api.signInAdmin(signInPayload, {
      loading: (val) => null,
      error: (err) => signInError(),
      success: (res) => {
        store.token = res.token;
        store.isAdmin = true;
        history.push("/admin");
      },
    })
  }

  const signIn = (signInPayload) => {
    api.signIn(
      signInPayload,
      {
        loading: (val) => null,
        error: (err) => signInAdmin(signInPayload),
        success: (res) => {
          store.token = res.token;
          whoAmI();
        },
      }
    );
  };

  const whoAmI = () => {
    api.whoAmI({
      loading: (val) => null,
      error: (err) => {
        signInError()
      },
      success: (res) => {
        store.restaurant = res;
        initializeWs();
        history.push("/home");
      },
    });
  }

  const form = useFormik({
    validationSchema: yup.object().shape({
      email: yup.string().email("Invalid email").required("Required"),
      password: yup.string().required("Required"),
    }),
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: (values) => signIn(values)
  });

  return (
    <Container fluid className="d-flex vh-100 bg-black overflow-auto p-0 align-items-stretch">
      <Col md={5} className="bg-danger" style={{ backgroundImage: `url(${require("../../../assets/good-food.svg")})` }} />
      <Col md={7} className="p-0">
        <div className="bg-white rounded-extra h-100 d-flex flex-column align-items-center py-5">
          <Col md={10} sm={12} lg={8} className="flex-basis-0 d-flex align-items-start justify-content-between">
            <h3 className="font-semi-bold m-0">{t("signIn.title")}</h3>

            <img src={image} style={{ height: "4em" }} />
          </Col>
          <Form className="d-flex flex-column flex-grow-1 justify-content-center w-100">
            <div className="px-5 h-100 w-100 d-flex flex-column justify-content-center align-items-center">


              <Col md={10} sm={12} lg={6} className="flex-basis-0" >
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      type="text"
                      name="email"
                      value={form.values.email}
                      onChange={form.handleChange}
                      isInvalid={!!form.touched.email && !!form.errors.email}
                    />
                    <Form.Control.Feedback type="invalid">
                      {form.errors.email}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Form.Row>

                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Label>{t("signIn.password")}</Form.Label>
                    <Form.Control
                      type="password"
                      name="password"
                      value={form.values.password}
                      onChange={form.handleChange}
                      isInvalid={!!form.touched.password && !!form.errors.password}
                      onKeyDown={(e) => e.key === 'Enter' && form.submitForm()}
                    />
                    <Form.Control.Feedback type="invalid">
                      {form.errors.password}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Form.Row>
              </Col>
            </div>
          </Form>
          <Col md={10} sm={12} lg={8} className="flex-basis-0 d-flex">

            <div className="ml-auto d-flex flex-column align-items-end">
              <div className="d-flex mb-2">
                <Button
                  onClick={() => form.submitForm()}
                  variant="primary"
                >
                  {t("signIn.submit")}
                </Button>
              </div>
              <Link to="/signup" className="text-light-gray font-small">
                {t("signIn.noAccount")}
              </Link>
            </div>
          </Col>
        </div>
      </Col >
    </Container>
  );
};

export default SignUp;
