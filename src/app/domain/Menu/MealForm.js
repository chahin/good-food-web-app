import React, { useEffect, useMemo, useRef, useState } from "react";
import { Form, Col, Button } from "react-bootstrap"
import { useFormik } from "formik";
import * as yup from "yup";
import IconSelect, { ICONS } from "app/components/IconSelect";
import { AddPhotoAlternateRounded, DeleteRounded, EuroRounded } from "@material-ui/icons";
import { useTranslation } from 'react-i18next';

const MealForm = function (props) {
    const { t } = useTranslation();
    const imageInput = useRef(null);
    const [imageUrl, setImageUrl] = useState(null);
    const [isDeleteImage, setIsDeleteImage] = useState(false);

    useEffect(() => {
        if (props?.meal?.image) {
            setImageUrl(props.meal.image);
        }
    }, []);

    const form = useFormik({
        validationSchema: yup.object().shape({
            name: yup.string().required(),
            description: yup.string().nullable(),
            price: yup.number().required().positive(),
            isAvailable: yup.bool().required().default(true),
            image: yup.mixed(),
            icon: yup.string().required().oneOf(ICONS),
        }),
        initialValues: props.meal ? {
            id: props.meal.id,
            name: props.meal.name,
            description: props.meal.description,
            isAvailable: props.meal.isAvailable,
            price: props.meal.price,
            image: props.meal.image,
            icon: props.meal.icon
        } : {
            name: "",
            description: null,
            isAvailable: true,
            price: null,
            image: null,
            icon: ICONS[0]
        },
        onSubmit: values => {
            // const filteredValues = Object.fromEntries(Object.entries(values).filter(([key]) => key !== "image"));
            props.onSubmit(
                props.meal
                    ? Object.fromEntries(Object.entries(values).filter(([key, value]) => form.initialValues[key] !== value))
                    : values,
                isDeleteImage
            )
        },
    });

    useEffect(() => {
        if (props.validated) {
            form.submitForm();
            props.onResetValidation();
        }
    }, [props.validated]);

    const loadImage = (e) => {
        form.setValues({ ...form.values, image: e.target.files[0] });
        setImageUrl(URL.createObjectURL(e.target.files[0]));
    }

    const unloadImage = (e) => {
        form.setValues({ ...form.values, image: null });
        setImageUrl(null);
        imageInput.current.value = null;
        if (props.meal?.image)
            setIsDeleteImage(true);
    }

    return (
        <Form className="p-3 d-flex flex-column flex-grow-1">
            <Form.Row>
                <Form.Group as={Col} md={8}>
                    <Form.Label>{t("menu.name")}</Form.Label>
                    <Form.Control
                        type="text"
                        name="name"
                        value={form.values.name}
                        onChange={form.handleChange}
                        isInvalid={form.touched.name && !!form.errors.name}
                    />
                </Form.Group>
                <Form.Group as={Col} md={4}>
                    <Form.Label className="d-flex align-items-center">
                        {t("menu.initialPrice")}
                        <EuroRounded className="ml-2 mb-n1 text-gray" style={{ fontSize: "1.2rem" }} />
                    </Form.Label>
                    <Form.Control
                        type="number"
                        name="price"
                        placeholder={t("menu.price")}
                        value={form.values.price || ""}
                        onChange={form.handleChange}
                        isInvalid={form.touched.price && !!form.errors.price}
                        min="0"
                    />
                </Form.Group>
            </Form.Row>
            <Form.Row>
                <Col className="d-flex">
                    <Form.Group className="mr-2">
                        <Form.Label>{t("menu.icon")}</Form.Label>
                        <IconSelect onSelect={(icon) => form.setValues({ ...form.values, icon })} value={form.values.icon} />
                    </Form.Group>
                    <Form.Group className="flex-grow-1">
                        <Form.Label>{t("menu.description")}</Form.Label>
                        <Form.Control
                            as="textarea"
                            name="description"
                            className="meal-description"
                            value={form.values.description ?? ""}
                            onChange={form.handleChange}
                            isInvalid={form.touched.description && !!form.errors.description} />
                    </Form.Group>
                </Col>
            </Form.Row>
            <Form.Row>
                <Form.Group as={Col} md="12">
                    <Form.Check
                        id="isAvailable"
                        name="isAvailable"
                        type="switch"
                        checked={form.values.isAvailable}
                        label={t("menu.available")}
                        onChange={form.handleChange}
                        isInvalid={form.touched.isAvailable && !!form.errors.isAvailable} />
                </Form.Group>
            </Form.Row>
            {imageUrl ? (
                <Form.Row className="flex-grow-1 mb-2">
                    <Col md={12} className="d-flex flex-column">
                        <div
                            style={{ backgroundImage: `url(${imageUrl})` }}
                            className="meal-image p-2 d-flex justify-content-end"
                        >
                            <Button
                                className="btn-square"
                                onClick={unloadImage}
                            >
                                <DeleteRounded />
                            </Button>
                        </div>
                    </Col>
                </Form.Row>
            ) : (
                <Form.Row>
                    <Col md={12} className="d-flex flex-column mb-2">
                        <Button
                            variant={form.touched.image && form.errors.image ? "outline-danger" : "secondary"}
                            className="d-flex w-100 justify-content-center align-items-center"
                            onClick={() => imageInput.current.click()}
                        >
                            <AddPhotoAlternateRounded />
                            <span className="ml-2">{t("menu.addImage")}</span>
                        </Button>
                    </Col>
                </Form.Row>
            )}
            <Form.Control
                ref={imageInput}
                type="file"
                name="image"
                accept="image/png, image/jpeg"
                onChange={loadImage}
            />
        </Form>
    )
};

export default MealForm;