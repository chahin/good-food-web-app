import React, { useMemo, useEffect, useContext } from "react";
import { useState } from "react";
import { Tooltip, Accordion, Button, Form, Modal, ListGroup, Container, Row, Col, Card, Badge, Carousel, OverlayTrigger } from "react-bootstrap";
import { ArrowForwardRounded, DoneRounded, ArrowBackRounded, AddRounded, ImageRounded, EditRounded, DeleteRounded } from "@material-ui/icons";
import MealForm from "./MealForm";
import { useLocation } from "react-router";

import "react-step-progress-bar/styles.css";
import { ProgressBar, Step } from "react-step-progress-bar";
import ModifierForm from "./ModifierForm";
import { v4 as uuidv4 } from 'uuid';
import api from "../../service/api";
import { AlertContext } from "app/context/AlertContext";
import ConfirmModal from "app/components/ConfirmModal";
import { useTranslation } from 'react-i18next';
const TOTAL_STEP = 2;

const Menu = function () {
    const { t } = useTranslation();
    const location = useLocation();
    const [sections, setSections] = useState([]);
    const [selectedSection, setSelectedSection] = useState(null);
    const [sectionTitle, setSectionTitle] = useState("");
    const [currentStep, setCurrentStep] = useState(0);

    const [mealFormValidated, setMealFormValidated] = useState(false);
    const [modifierFormValidated, setModifierFormValidated] = useState(false);
    const [onlyStepOne, setOnlyStepOne] = useState(false);
    const [imageInstruction, setImageInstruction] = useState(null);

    const [meal, setMeal] = useState(null);

    const isFirstStep = useMemo(() => currentStep === 0, [currentStep]);
    const isLastStep = useMemo(() => currentStep === TOTAL_STEP - 1, [currentStep]);
    const showAlert = useContext(AlertContext);
    const [editSectionTitle, setEditSectionTitle] = useState(null);
    const [isEditMode, setIsEditMode] = useState(false);

    const [confirmModalState, setConfirmModalState] = useState(null);

    useEffect(() => {
        if (meal && (onlyStepOne || isLastStep)) {
            handleModalClose();
            addMealToSection(meal);
            setOnlyStepOne(false);
        }
    }, [meal]);

    useEffect(() => {
        if (selectedSection === null)
            setMeal(null);
    }, [selectedSection])

    useEffect(() => {
        if (location.pathname === "/menu") {
            getMenu();
            setImageInstruction(null);
        }
    }, [location]);

    const getMenu = () => {
        api.whoAmI(
            {
                loading: (val) => { },
                error: (err) => console.log(err),
                success: (res) => setSections(res.food)
            }
        )
    }

    const saveMealImage = (image) => {
        const data = new FormData();
        data.append(meal.id, image);
        api.uploadMealImage(
            { sectionId: selectedSection.id, mealId: meal.id },
            data,
            {
                loading: (val) => { },
                error: (err) => showAlert({ message: t("menu.couldNotSaveMenu"), type: "error" }),
                success: (updatedMeal) => {
                    notifyMealState(updatedMeal);
                    showAlert({ message: t("menu.menuSaved") })
                }
            }
        );
    }

    const removeMealImage = () => {
        api.deleteMealImage(
            { sectionId: selectedSection.id, mealId: meal.id },
            {
                loading: (val) => { },
                error: (err) => showAlert({ message: t("menu.couldNotSaveMenu"), type: "error" }),
                success: (updatedMeal) => {
                    notifyMealState(updatedMeal);
                    showAlert({ message: t("menu.menuSaved") })
                }
            }
        );
    }

    const saveMenu = (food) => {
        setSections([...food]);
        api.updateRestaurant(
            { food },
            {
                loading: (val) => { },
                error: (err) => showAlert({ message: t("menu.couldNotSaveMenu"), type: "error" }),
                success: async (res) => {
                    if (imageInstruction) {
                        if (imageInstruction.isDelete)
                            await removeMealImage();
                        else
                            await saveMealImage(imageInstruction.image);
                    } else
                        showAlert({ message: t("menu.menuSaved") });
                    setMeal(null);
                    setImageInstruction(null);
                }
            }
        )
    }

    const openMealForm = (sId) => setSelectedSection(sections.find(s => s.id === sId));
    const handleModalClose = () => {
        setSelectedSection(null);
        setIsEditMode(false);
    }
    const resetStep = () => setCurrentStep(0);
    const previousStep = () => setCurrentStep(isFirstStep ? 0 : currentStep - 1);

    const addSection = () => {
        if (sectionTitle.trim().length > 0) {
            saveMenu([...sections, {
                id: uuidv4(),
                name: sectionTitle,
                content: []
            }]);
            setSectionTitle('');
        }
    }

    const removeSection = (sectionId) => {
        setConfirmModalState({
            show: true,
            onSubmit: () => saveMenu(sections.filter(s => s.id !== sectionId)),
        });
    }

    const notifyMealState = (m) => {
        const indexToUpdate = selectedSection.content.map(m => m.id).indexOf(m.id);
        selectedSection.content[indexToUpdate] = m;
        setSections([...sections]);
    }

    const addMealToSection = (m) => {
        if (isEditMode) {
            const indexToUpdate = selectedSection.content.map(m => m.id).indexOf(m.id);
            selectedSection.content[indexToUpdate] = m;
            saveMenu(sections);
        } else {
            selectedSection.content.push(m);
            saveMenu(sections);
        }
    }

    const removeMeal = (sectionId, mealId) => {
        setConfirmModalState({
            show: true,
            onSubmit: () => {
                const section = sections.find(s => s.id === sectionId);
                section.content = section.content.filter(m => m.id !== mealId);
                saveMenu(sections);
            },
        });
    }

    const submitFirstStep = (mealData, isDeleteImage) => {
        if (mealData.image || isDeleteImage)
            setImageInstruction(mealData.image ? { image: mealData.image } : { isDelete: isDeleteImage });
        delete mealData.image;
        setMeal({
            id: mealData.id || uuidv4(),
            ...meal,
            ...mealData
        });
        if (onlyStepOne)
            setSelectedSection(null)
        else
            setCurrentStep(currentStep + 1);
    }

    const submitFirstStepAndClose = () => {
        setOnlyStepOne(true);
        setMealFormValidated(true);
    }

    const submitLastStep = (modifierData) => {
        let newMeal = { ...meal };
        if (modifierData?.length > 0)
            newMeal = { ...meal, content: modifierData };
        setMeal(newMeal);
    }

    const renderTooltip = (props, text) => {
        return (
            <Tooltip id="button-tooltip" {...props} className="rounded-extra">
                {text}
            </Tooltip>
        );
    }


    const setEndOfContenteditable = (contentEditableElement) => {
        let range, selection;
        if (document.createRange) {
            range = document.createRange();
            range.selectNodeContents(contentEditableElement);
            range.collapse(false);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        }
        else if (document.selection) {
            range = document.body.createTextRange();
            range.moveToElementText(contentEditableElement);
            range.collapse(false);
            range.select();
        }
    }

    return <>
        <Container fluid className="h-100 overflow-x-hidden overflow-auto p-4">
            <h4 className="font-bold m-0 py-2 mb-3">{t("menu.title")}</h4>
            <Row>
                <Col md={6} lg={4}>
                    <div className="mt-3 ml-4 bg-white p-3 rounded-extra d-flex flex-column shadow">
                        <Form.Control
                            className="mb-2"
                            placeholder={t("menu.placeholder")}
                            aria-label="Section title"
                            aria-describedby="section-title"
                            value={sectionTitle}
                            onChange={e => setSectionTitle(e.target.value)}
                        />
                        <Button variant="dark" onClick={addSection} size="sm">{t("menu.addSection")}</Button>
                    </div>
                </Col>
            </Row>
            {/* <Container> */}
            {
                sections.map(s => (
                    <Row className="ml-3 mt-4" key={s.id}>
                        <Col lg={8}>
                            <div className="d-flex align-items-center mb-2">
                                <h5
                                    id={s.id}
                                    className="section-title font-semi-bold m-0"
                                    contentEditable={editSectionTitle === s.id}
                                    suppressContentEditableWarning={true}
                                    onFocus={(e) => setEndOfContenteditable(e.target)}
                                    onKeyDown={(e) => {
                                        if (e.keyCode == 13) {
                                            e.preventDefault();
                                            if (editSectionTitle === s.id && e.target.textContent.trim().length > 0) {
                                                sections.find(section => section.id === s.id).name = e.target.textContent;
                                                saveMenu(sections);
                                                setEditSectionTitle(null);
                                            }
                                        }
                                    }}
                                    onBlur={(e) => {
                                        if (editSectionTitle === s.id) {
                                            e.target.textContent = s.name;
                                            setEditSectionTitle(null);
                                        }
                                    }}
                                >
                                    {s.name}
                                </h5>

                                <OverlayTrigger
                                    placement="top"
                                    delay={{ show: 500, hide: 0 }}
                                    overlay={(props) => renderTooltip(props, t("menu.renameSection"))}
                                >
                                    <Button
                                        className="btn-square ml-2"
                                        variant="outline-primary"
                                        size="sm"
                                        onMouseDown={() => {
                                            const titleElement = document.getElementById(s.id);
                                            if (editSectionTitle === s.id && titleElement.textContent.trim().length > 0) {
                                                sections.find(section => section.id === s.id).name = titleElement.textContent;
                                                saveMenu(sections);
                                                setEditSectionTitle(null);
                                            }
                                        }}
                                        onClick={() => {
                                            const titleElement = document.getElementById(s.id);
                                            if (editSectionTitle !== s.id) {
                                                setEditSectionTitle(s.id);
                                                setTimeout(() => titleElement.focus())
                                            }
                                        }}
                                    >
                                        {
                                            editSectionTitle === s.id
                                                ? <DoneRounded fontSize="small" />
                                                : <EditRounded fontSize="small" />
                                        }

                                    </Button>
                                </OverlayTrigger>

                                <OverlayTrigger
                                    placement="top"
                                    delay={{ show: 500, hide: 0 }}
                                    overlay={(props) => renderTooltip(props, t("menu.deleteSection"))}
                                >
                                    <Button className="btn-square ml-2" variant="outline-primary" onClick={() => removeSection(s.id)} size="sm">
                                        <DeleteRounded fontSize="small" />
                                    </Button>
                                </OverlayTrigger>

                                <OverlayTrigger
                                    placement="top"
                                    delay={{ show: 500, hide: 0 }}
                                    overlay={(props) => renderTooltip(props, t("menu.add"))}
                                >
                                    <Button variant="outline-primary" className="p-0 ml-2 btn-square" onClick={() => openMealForm(s.id)} size="sm">
                                        <AddRounded fontSize="small" />
                                    </Button>
                                </OverlayTrigger>

                            </div>
                            <Card className="bg-white shadow rounded-extra border-0">
                                <Card.Body className="p-2">
                                    {
                                        s.content.length > 0
                                            ? <ListGroup variant="flush">
                                                {
                                                    s.content.map(m => (
                                                        <ListGroup.Item key={m.id}>
                                                            <div className="section-child" key={m.id}>
                                                                <div className="meal-icon mr-2">
                                                                    {m.icon}
                                                                </div>
                                                                <div className="d-flex align-items-center">
                                                                    <div className="d-flex flex-column">
                                                                        <span className="text-gray font-semi-bold d-flex align-items-center">
                                                                            {m.name}
                                                                            {m.content && <OverlayTrigger placement="top" delay={{ show: 500, hide: 0 }} overlay={(props) => renderTooltip(props, m.content.map(modifier => modifier.name).join(", "))}>
                                                                                <Badge className="ml-2 cursor-help" variant="primary">{m.content.length} {m.content.length > 1 ? "options" : "option"}</Badge>
                                                                            </OverlayTrigger>
                                                                            }
                                                                            {!m.isAvailable && <Badge className="ml-2" variant="danger">{t("menu.unavailable")}</Badge>}
                                                                            {!m.image && <Badge className="ml-2" variant="primary">{t("menu.noImage")}</Badge>}
                                                                        </span>
                                                                        <span className="text-muted font-small">{m.description}</span>
                                                                    </div>
                                                                    <span className="ml-auto mr-3">{m.price.toFixed(2)} €</span>
                                                                    <Button
                                                                        className="btn-square"
                                                                        variant="outline-secondary"
                                                                        onClick={() => {
                                                                            setMeal(m);
                                                                            openMealForm(s.id);
                                                                            setIsEditMode(true);
                                                                        }}>
                                                                        <EditRounded />
                                                                    </Button>
                                                                    <Button
                                                                        className="btn-square ml-2"
                                                                        variant="delete"
                                                                        onClick={() => removeMeal(s.id, m.id)}
                                                                    >
                                                                        <DeleteRounded />
                                                                    </Button>
                                                                </div>
                                                            </div>
                                                        </ListGroup.Item>
                                                    ))
                                                }
                                            </ListGroup>
                                            : <div className="text-muted py-3 px-4">{t("menu.nothingYet")}</div>
                                    }
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                ))
            }
            {/* </Container> */}
        </Container>

        {/* Modal */}
        <Modal
            show={selectedSection !== null}
            onHide={handleModalClose}
            onExited={resetStep}
            backdrop="static"
            centered
            scrollable
            contentClassName={`h-100 ${currentStep === 1 ? "mh-100" : "mh-75"}`}
        >
            <Modal.Header>
                <div className="d-flex flex-column align-items-center justify-content-center w-100">
                    {selectedSection && <span className="font-semi-bold m-0">
                        {isEditMode ? `${t("menu.edit")} ${meal.name}` : `${t("menu.addElementIn")} "${selectedSection.name}"`}
                    </span>}
                    <div className="w-50">
                        <StepProgress step={currentStep} />
                    </div>
                </div>
            </Modal.Header>
            <Modal.Body className="p-0 bg-light">
                <Carousel
                    className="h-100 overflow-y-visible"
                    activeIndex={currentStep}
                    controls={false}
                    indicators={false}
                    interval={null}
                >

                    <Carousel.Item>
                        <MealForm
                            meal={meal}
                            validated={mealFormValidated}
                            onResetValidation={() => setMealFormValidated(false)}
                            onSubmit={submitFirstStep}
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <ModifierForm
                            meal={meal}
                            validated={modifierFormValidated}
                            onResetValidation={() => setModifierFormValidated(false)}
                            onSubmit={submitLastStep}
                        />
                    </Carousel.Item>
                </Carousel>
            </Modal.Body>
            <Modal.Footer className="d-flex">
                <Button variant="dark" onClick={handleModalClose}>
                    {t("menu.cancel")}
                </Button>
                <Button
                    onClick={() => isLastStep ? previousStep() : setMealFormValidated(true)}
                    variant="primary"
                    className="ml-auto"
                >
                    {
                        isLastStep
                            ? (
                                <div className="d-flex align-items-center">
                                    <ArrowBackRounded className="mr-2" />
                                    <div>{t("menu.previous")}</div>
                                </div>
                            )
                            : (
                                <div className="d-flex align-items-center">
                                    <div>{t("menu.next")}</div>
                                    <ArrowForwardRounded className="ml-2" />
                                </div>
                            )
                    }
                </Button>
                <Button
                    className="d-flex ml-2"
                    onClick={() => isLastStep ? setModifierFormValidated(true) : submitFirstStepAndClose()}
                    variant="primary"
                >
                    {t("menu.submit")}
                </Button>
            </Modal.Footer>
        </Modal>

        <ConfirmModal
            show={confirmModalState?.show}
            onSubmit={confirmModalState?.onSubmit}
            onHide={() => setConfirmModalState({ show: false, onSubmit: () => { } })} />
    </>
};


const StepProgress = function (props) {
    const { t } = useTranslation();
    // @ts-ignore
    return <ProgressBar
        unfilledBackground="#ced4da"
        filledBackground="#ffc107"
        height={5}
        className="w-50"
        percent={props.step === 0 ? 0 : 100}
    >
        {/* @ts-ignore */}
        <Step>
            {({ accomplished, index }) => (
                <div
                    className={`indexedStep ${accomplished ? "accomplished" : null} ${props.step === 0 ? "active-step" : null}`}
                >
                    <div>{index + 1}</div>
                    <div>{t("menu.description")}</div>
                </div>
            )}
        </Step>
        {/* @ts-ignore */}
        <Step>
            {({ accomplished, index }) => (
                <div
                    className={`indexedStep ${accomplished ? "accomplished" : null} ${props.step === 1 ? "active-step" : null}`}
                >
                    <div>{index + 1}</div>
                    <div>(Options)</div>
                </div>
            )}
        </Step>
    </ProgressBar>
};

export default Menu;
