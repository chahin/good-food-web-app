import React, { useContext, useEffect, useMemo, useRef } from "react";
import { useState } from "react";
// @ts-ignore
import { Accordion, AccordionContext, Badge, Button, Card, Col, Form, InputGroup, Overlay, OverlayTrigger, Row, Tooltip, useAccordionToggle } from "react-bootstrap"
import * as yup from "yup";
import { AddRounded, DeleteRounded, EditRounded, HighlightOffRounded } from "@material-ui/icons";
import { v4 as uuidv4 } from 'uuid';
import { useFormik } from "formik";
import { Animated } from "react-animated-css";
import { useTranslation } from 'react-i18next';

const ModifierForm = function (props) {
    // @ts-ignore
    const [modifiers, setModifiers] = useState(props.meal?.content || []);
    const [validated, setValidated] = useState(false);
    const [choicesAreValid, setChoicesAreValid] = useState(true);
    const [choices, setChoices] = useState([getDefaultChoice()]);
    const container = useRef(null);
    const [editedModifier, setEditedModifier] = useState(null);

    const { t } = useTranslation();

    const formik = useFormik({
        validationSchema: yup.object().shape({
            name: yup.string().required(),
            minimum: yup.number().required(),
            maximum: yup.number().required().min(yup.ref("minimum")).max(choices.length),
        }),
        initialValues: {
            name: "",
            minimum: 1,
            maximum: 1,
        },
        onSubmit: values => { }
    });

    useEffect(() => {
        if (props.validated) {
            // formik.validateForm().then(errors => {
            //     const isValid = Object.keys(errors).length === 0;
            //     if (isValid) {
            //         props.onSumbit(formik.values);
            //         formik.resetForm();
            //     }
            //     props.onResetValidation();
            // });
            props.onSubmit(modifiers);
            props.onResetValidation();
        }
    }, [props.validated]);

    function validateForm() {
        formik.submitForm();
        setChoicesAreValid(true);
        setValidated(true);
    }

    function getDefaultChoice() {
        return {
            id: uuidv4(),
            name: "",
            price: 0,
            isAvailable: true,
        }
    }

    function addInputChoice() {
        setChoices([
            ...choices,
            getDefaultChoice()
        ]);
    }

    function addModifier() {
        if (choicesAreValid && formik.isValid) {
            if (editedModifier) {
                const index = modifiers.map(m => m.id).indexOf(editedModifier);
                modifiers[index] = {
                    id: editedModifier,
                    ...formik.values,
                    content: choices
                }
                setModifiers([...modifiers]);
            } else {
                setModifiers([
                    ...modifiers,
                    {
                        id: uuidv4(),
                        ...formik.values,
                        content: choices
                    }
                ]);
            }
            resetForm();
        }
    }

    function resetForm() {
        setChoices([getDefaultChoice()]);
        formik.resetForm();
        setEditedModifier(null);
    }

    function removeInputChoice(id) {
        if (choices.length > 1)
            setChoices(choices.filter(ic => ic.id !== id));
    }

    function removeModifier(id) {
        setModifiers(modifiers.filter(m => m.id !== id));
    }

    function editModifier(m) {
        setEditedModifier(m.id);
        const { content, ...rest } = m;
        setChoices(content);
        formik.setValues({ ...rest });
        container.current.scrollTo({ top: 0, behavior: "smooth" });
    }

    return (
        <div className="p-3 overflow-auto" ref={container}>
            <Card className="bg-transparent border-dashed">
                <Card.Body>
                    <Form.Group>
                        <Form.Label>{t("menu.optionName")}</Form.Label>
                        <Form.Control
                            className="mr-3"
                            size="sm"
                            placeholder={t("menu.modifierPlaceholder")}
                            name="name"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            isInvalid={formik.touched.name && !!formik.errors.name}
                        />
                    </Form.Group>
                    <Row>
                        <Form.Group as={Col}>
                            <Form.Label>{t("menu.numberOfChoices")}</Form.Label>
                            <div className="d-flex align-items-center">
                                <Form.Control
                                    type="number"
                                    name="minimum"
                                    size="sm"
                                    className="input-width-sm"
                                    placeholder="Min"
                                    value={formik.values.minimum}
                                    onChange={formik.handleChange}
                                    isInvalid={formik.touched.minimum && !!formik.errors.minimum}
                                    min="0"
                                />
                                <Form.Control
                                    type="number"
                                    name="maximum"
                                    size="sm"
                                    className="input-width-sm ml-2"
                                    placeholder="Max"
                                    value={formik.values.maximum}
                                    onChange={formik.handleChange}
                                    isInvalid={formik.touched.maximum && !!formik.errors.maximum}
                                    min="0"
                                />
                            </div>
                        </Form.Group>

                    </Row>
                    <Row>
                        <Form.Group as={Col}>
                            <Form.Label>
                                {t("menu.choices")}
                                <span className="indication-text ml-2">{t("menu.choiceDesc")}</span>
                            </Form.Label>

                            {
                                choices.map((c, index) => (
                                    <InputChoice
                                        key={c.id}
                                        choice={c}
                                        removable={choices.length > 1}
                                        validated={validated}
                                        onResetValidation={() => setValidated(false)}
                                        onRemove={removeInputChoice}
                                        onSubmit={(choice, isValid) => {
                                            if (!isValid)
                                                setChoicesAreValid(false);
                                            else {
                                                choices[index] = { ...choices[index], ...choice };
                                                setChoices(choices);
                                                if (index === choices.length - 1)
                                                    addModifier();
                                            }
                                        }}
                                    />
                                ))
                            }

                            <Button
                                size="sm"
                                variant="outline-secondary"
                                onClick={addInputChoice}
                                className="btn-block p-0 py-1"
                            >
                                <AddRounded />
                            </Button>
                        </Form.Group>
                    </Row>

                    <Button
                        className="btn-block"
                        size="sm"
                        variant="dark"
                        onClick={validateForm}
                    >
                        {editedModifier ? t("menu.save") : t("menu.add")}
                    </Button>
                    {
                        editedModifier && <Button
                            className="btn-block"
                            size="sm"
                            variant="secondary"
                            onClick={resetForm}
                        >
                            {t("menu.cancelEdit")}
                        </Button>
                    }
                </Card.Body>
            </Card>
            {
                modifiers.map(m => (
                    <Animated key={m.id} animationIn="fadeInUp" animationInDuration={300} animationOut="fadeOut" animationInDelay={350} isVisible={true}>
                        <ModifierCard modifier={m} onRemove={() => removeModifier(m.id)} onEdit={() => editModifier(m)} />
                    </Animated>
                ))
            }
        </div>
    )
}



const ModifierCard = function (props) {
    const { t } = useTranslation();

    return (
        <Card className="mt-3 rounded-extra modifier-card">
            <Card.Body className="p-3">
                <div className="d-flex justify-content-between">
                    <div className="d-flex align-items-center">
                        <label className="text-dark mb-0">{props.modifier.name}</label>
                        {props.modifier.minimum === 1 && <Badge className="ml-1" variant="primary">{t("menu.required")}</Badge>}
                        {props.modifier.minimum > 1 && <Badge className="ml-1" variant="primary">Min {props.modifier.minimum}</Badge>}
                        {props.modifier.maximum > 1 && <Badge className="ml-1" variant="primary">Max {props.modifier.maximum}</Badge>}
                    </div>
                    <div className="d-flex align-items-center">
                        <Button className="d-flex align-items-center font-smaller" variant="outline-secondary" size="sm" onClick={props.onEdit}>
                            <EditRounded />
                            <div className="ml-1">{t("menu.edit")}</div>
                        </Button>
                        <Button
                            className="d-flex align-items-center ml-2"
                            onClick={() => props.onRemove()}
                            variant="delete"
                            size="sm"
                        >
                            <DeleteRounded />
                            <div className="ml-1">{t("menu.delete")}</div>
                        </Button>
                    </div>
                </div>
                {
                    props.modifier.content.map(c => (
                        <div
                            key={c.id}
                            className={`font-small d-flex align-items-center rounded-high mt-2 py-1 px-3 ${!c.isAvailable ? "bg-danger-light text-danger" : "bg-light text-gray"}`}>
                            <div className={`font-small ${!c.isAvailable ? "text-danger" : ""}`}>{c.name}</div>
                            <div className="ml-auto font-small">{`${c.price.toFixed(2)} €`}</div>
                        </div>
                    ))
                }
            </Card.Body>
        </Card>
    );
};


const InputChoice = function (props) {
    const [hasPrice, setHasPrice] = useState(props.choice?.price > 0);
    const [mounted, setMounted] = useState(false);
    const { t } = useTranslation();

    const formik = useFormik({
        validationSchema: yup.object().shape({
            name: yup.string().required(),
            price: hasPrice ? yup.number().positive().required() : null,
            isAvailable: yup.bool().required(),
        }),
        initialValues: {
            name: props.choice.name || "",
            price: hasPrice ? props.choice.price ?? null : 0,
            isAvailable: props.choice.isAvailable !== undefined ? props.choice.isAvailable : true
        },
        onSubmit: values => props.onSubmit(formik.values, true)
    });

    useEffect(() => {
        setMounted(true);
        return () => setMounted(false);
    }, [])

    useEffect(() => {
        if (mounted && props.validated) {
            formik.submitForm();
            formik.validateForm().then(errors => {
                const isValid = Object.keys(errors).length === 0;
                props.onResetValidation();
                if (isValid)
                    formik.resetForm();
                else
                    props.onSubmit(formik.values, false);
            });
        }
    }, [props.validated]);

    return (
        <div className="d-flex align-items-center mb-2">
            <Form.Check
                id={`choiceIsAvailable${props.choice.id}`}
                name="isAvailable"
                type="switch"
                checked={formik.values.isAvailable}
                onChange={formik.handleChange}
                isInvalid={formik.touched.isAvailable && !!formik.errors.isAvailable}
            />

            <Form.Control
                size="sm"
                className="flex-grow-1"
                placeholder={t("menu.name")}
                value={formik.values.name}
                name="name"
                onChange={formik.handleChange}
                isInvalid={formik.touched.name && !!formik.errors.name}
            />

            {
                hasPrice ? (
                    <div className="input-button-group d-flex align-items-center">
                        <Form.Control
                            type="number"
                            name="price"
                            size="sm"
                            className="input-width-sm ml-2"
                            placeholder={`${t("menu.price")} (€)`}
                            value={formik.values.price}
                            onChange={formik.handleChange}
                            isInvalid={formik.touched.price && !!formik.errors.price}
                            min="0" />
                        <Button
                            size="sm"
                            variant="outline-secondary"
                            onClick={() => {
                                setHasPrice(false);
                                formik.values.price = 0;
                            }}
                        >
                            <HighlightOffRounded />
                        </Button>
                    </div>
                ) : (
                    <Button
                        className="ml-2 text-nowrap"
                        size="sm"
                        variant="outline-secondary"
                        onClick={() => setHasPrice(true)}
                    >
                        {t("menu.addPrice")}
                    </Button>
                )
            }

            <Button
                disabled={!props.removable}
                className="btn-square ml-2"
                size="sm"
                variant="delete"
                onClick={() => props.onRemove(props.choice.id)}
            >
                <DeleteRounded />
            </Button>
        </div>
    );
}

export default ModifierForm;