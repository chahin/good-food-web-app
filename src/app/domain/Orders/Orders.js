import React, { useState, useEffect } from "react";
import { Tab, Nav, Container, Row, Col } from "react-bootstrap";
import { FastfoodRounded, LocalMallRounded, ReorderRounded } from "@material-ui/icons/";
import New from "./New";
import Current from "./InPreparation";
import Sent from "./Sent";
import { useTranslation } from 'react-i18next';


const Orders = function () {
    const { t } = useTranslation();

    const [key, setKey] = useState("new");

    return (
        <Container fluid className="h-100 overflow-x-hidden overflow-auto p-4 d-flex flex-column">
            <h4 className="font-bold m-0 py-2 mb-3">{t("orders.title")}</h4>
            <Row className="mx-3 mt-4 flex-grow-1">
                <Col lg={9} className="h-100 d-flex flex-column">
                    <Tab.Container
                        defaultActiveKey="new"
                        transition={false}
                        onSelect={(k) => setKey(k)}
                    >
                        <Nav className="order-nav" variant="tabs" fill>
                            <Nav.Item>
                                <Nav.Link eventKey="new" className="tab-title">
                                    <ReorderRounded />
                                    <span>{t("orders.new")}</span>
                                </Nav.Link>
                            </Nav.Item>

                            <Nav.Item>
                                <Nav.Link eventKey="inPreparation" className="tab-title">
                                    <FastfoodRounded />
                                    <span>{t("orders.inPreparation")}</span>
                                </Nav.Link>
                            </Nav.Item>

                            <Nav.Item>
                                <Nav.Link eventKey="sent" className="tab-title">
                                    <LocalMallRounded />
                                    <span>{t("orders.sent")}</span>
                                </Nav.Link>
                            </Nav.Item>
                        </Nav>

                        <div className={`order-nav-content ${key !== "new" ? "rounded-top-left" : ""} ${key !== "sent" ? "rounded-top-right" : ""}`}>
                            <Tab.Content className="w-100">
                                <Tab.Pane eventKey="new" className="h-100">
                                    <New namepage={key} />
                                </Tab.Pane>

                                <Tab.Pane eventKey="inPreparation" className="h-100">
                                    <Current namepage={key} />
                                </Tab.Pane>

                                <Tab.Pane eventKey="sent" className="h-100">
                                    <Sent namepage={key} />
                                </Tab.Pane>
                            </Tab.Content>
                        </div>
                    </Tab.Container>
                </Col>
            </Row>
        </Container>
    );
}

export default Orders;
