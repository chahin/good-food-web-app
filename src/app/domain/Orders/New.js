import React, { useState, useEffect, useContext } from "react";
import { Spinner } from "react-bootstrap";
import api from "app/service";
import OrderCard from "app/components/OrderCard";
import store from "app/store";
import { WsContext } from "app/context";
import { useTranslation } from 'react-i18next';

const New = function (props) {
  const { t } = useTranslation();
  const { newOrders, remove } = useContext(WsContext);
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (props.namepage === "new") {
      api.getOrders(
        { status: "waiting" },
        {
          loading: (val) => setLoading(val),
          error: (err) => console.log(err),
          success: (res) => {
            console.log(res);
            setOrders(res);
          }
        }
      )
      return () => setOrders([]);
    }
  }, [props.namepage]);

  useEffect(() => {
    setOrders(newOrders);
  }, [newOrders])

  const removeFromOrderState = (orderId) => {
    setOrders(orders.filter(o => o._id !== orderId));
  }

  return (
    <div className="d-flex flex-column h-100 overflow-auto justify-content-center align-items-center p-3">
      {
        loading ? (
          <Spinner animation="border" variant="primary" />
        ) : (
          orders.length > 0
            ? <div className="w-100 d-flex flex-wrap mb-auto">
              {orders.map(order => (
                <OrderCard
                  key={order._id}
                  order={order}
                  className="w-50"
                  nextStatus="inPreparation"
                  buttonLabel="Accept"
                  onRefuse={() => removeFromOrderState(order._id)}
                  onNextStatus={() => removeFromOrderState(order._id)}
                />
              ))}
            </div>
            : <div>{t("orders.empty")}</div>
        )
      }
    </div >
  );
};

export default New;