import React, { useState, useEffect } from "react";
import { Spinner } from "react-bootstrap";
import api from "app/service";
import OrderCard from "app/components/OrderCard";
import { useTranslation } from 'react-i18next';

const InPreparation = ({ namepage }) => {
  const { t } = useTranslation();
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (namepage === "inPreparation") {
      getOrdersInPreparation()
    }
    return () => setOrders([]);
  }, [namepage]);

  const getOrdersInPreparation = () => {
    api.getOrders(
      { status: "inPreparation" },
      {
        loading: (val) => setLoading(val),
        error: (err) => console.log(err),
        success: (res) => {
          console.log(res)
          setOrders(res);
        }
      }
    )
  }

  const removeFromOrderState = (orderId) => {
    setOrders(orders.filter(o => o._id !== orderId));
  }

  return (
    <div className="d-flex flex-column h-100 overflow-auto justify-content-center align-items-center p-3">
      {loading ? (
        <Spinner animation="border" variant="primary" />
      ) : (
        orders.length > 0
          ? <div className="w-100 d-flex flex-wrap mb-auto">
            {orders.map(order => (
              <OrderCard
                key={order._id}
                order={order}
                nextStatus="inDelivery"
                buttonLabel="Send"
                onRefuse={() => removeFromOrderState(order._id)}
                onNextStatus={() => removeFromOrderState(order._id)}
              />
            ))}
          </div>
          : <div>{t("orders.empty")}</div>
      )}
    </div>
  );
};

export default InPreparation;
