import React, { useEffect, useState } from "react";
import { Spinner } from "react-bootstrap";
import OrderCard from "app/components/OrderCard";
import api from "app/service";
import { useTranslation } from 'react-i18next';

const Sent = ({ namepage }) => {
  const { t } = useTranslation();
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (namepage === "sent") {
      getSentOrders();
    }
    return () => setOrders([]);
  }, [namepage]);

  const getSentOrders = () => {
    api.getOrders(
      { status: ["inDelivery", "delivered"] },
      {
        loading: (val) => setLoading(val),
        error: (err) => console.log(err),
        success: (res) => {
          console.log(res)
          setOrders(res);
        }
      }
    )
  }

  return (
    <div className="d-flex flex-column h-100 overflow-auto justify-content-center align-items-center p-3">
      {loading ? (
        <Spinner animation="border" variant="primary" />
      ) : (
        orders.length > 0
          ? <div className="w-100 d-flex flex-wrap mb-auto">
            {orders.map(order => (
              <OrderCard
                key={order._id}
                order={order}
                nextStep="inPreparation"
              />
            ))}
          </div>
          : <div>{t("orders.empty")}</div>
      )}
    </div>
  );
};

export default Sent;
