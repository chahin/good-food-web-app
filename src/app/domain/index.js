export { default as Home } from "./Home";
export { default as Main } from "./Main";
export { default as Orders } from "./Orders/Orders";
export { default as Menu } from "./Menu/Menu";
export { default as NotFound } from "./NotFound";
