import React, { useContext } from "react";
import { useState, useEffect } from "react";
import {
  Button,
  Card,
  Container,
  Tooltip,
} from "react-bootstrap";
import GetAppRoundedIcon from "@material-ui/icons/GetAppRounded";
import { useLocation } from "react-router-dom";
import api from "app/service";
import { AlertContext } from "app/context/AlertContext";
import fileDownload from "js-file-download";
import LoadingButton from "app/components/LoadingButton";
import { useTranslation } from 'react-i18next';

const Admin = function () {
  const { t } = useTranslation();
  const location = useLocation();
  const [restaurants, setRestaurants] = useState([]);
  const [approveLoading, setApproveLoading] = useState({});
  const [rejectLoading, setRejectLoading] = useState({});

  const showAlert = useContext(AlertContext);

  useEffect(() => {
    if (location.pathname === "/admin") {
      getAllUnverified()
    }
  }, [location]);

  const getAllUnverified = () => {
    api.getAllUnverified({
      loading: (val) => null,
      error: (err) => showAlert({ message: "Could not retrieve applications", type: "error" }),
      success: (res) => setRestaurants(res)
    })
  }

  const downloadId = (restaurantId) => {
    api.downloadId(
      restaurantId,
      {
        loading: (val) => null,
        error: (err) => showAlert({ message: "Could not download id document", type: "error" }),
        success: ({ file, filename }) => fileDownload(file, filename)
      }
    );
  }

  const downloadSiret = (restaurantId) => {
    api.downloadSiret(
      restaurantId,
      {
        loading: (val) => null,
        error: (err) => showAlert({ message: "Could not download SIRET document", type: "error" }),
        success: ({ file, filename }) => fileDownload(file, filename)
      }
    );
  }

  const approveRestaurant = (restaurantId) => {
    api.approveRestaurant(
      restaurantId,
      {
        loading: (val) => setApproveLoading({ ...approveLoading, [restaurantId]: val }),
        error: (err) => showAlert({ message: "Could not approve restaurant application", type: "error" }),
        success: (res) => {
          setRestaurants(restaurants.filter(r => r._id !== restaurantId));
          showAlert({ message: "Restaurant successfully approved" })
        }
      }
    )
  }

  const rejectRestaurant = (restaurantId) => {
    api.rejectRestaurant(
      restaurantId,
      {
        loading: (val) => setRejectLoading({ ...rejectLoading, [restaurantId]: val }),
        error: (err) => showAlert({ message: "Could not reject restaurant application", type: "error" }),
        success: (res) => {
          setRestaurants(restaurants.filter(r => r._id !== restaurantId));
          showAlert({ message: "Restaurant successfully rejected" });
        }
      }
    )
  }

  return (
    <Container fluid className="h-100 overflow-x-hidden p-3">
      <h4 className="font-semi-bold m-0 py-2 mb-3">{t("admin.applications")}</h4>

      {
        restaurants.map(restaurant => (
          <Card key={restaurant._id} className="bg-white shadow rounded-extra border-0 mb-3">
            <Card.Body>
              <h6 className="font-semi-bold mb-4">{restaurant.name}</h6>
              <ul className="text-gray">
                <li>{restaurant.address.label}</li>
                <li>{restaurant.category}</li>
                <li>{restaurant.email}</li>
              </ul>

              <div className="d-flex align-items-center justify-content-end">
                <Button
                  variant="secondary"
                  className="d-flex justify-content-center align-items-center"
                  onClick={() => downloadId(restaurant._id)}
                >
                  <GetAppRoundedIcon />
                  <span className="ml-2">id</span>
                </Button>
                <Button
                  variant="secondary"
                  className="d-flex justify-content-center align-items-center ml-2"
                  onClick={() => downloadSiret(restaurant._id)}
                >
                  <GetAppRoundedIcon />
                  <span className="ml-2">siret</span>
                </Button>

                <LoadingButton
                  className="ml-auto"
                  loading={rejectLoading[restaurant._id]}
                  onClick={() => rejectRestaurant(restaurant._id)}
                >
                  {t("admin.reject")}
                </LoadingButton>

                <LoadingButton
                  className="ml-2"
                  loading={approveLoading[restaurant._id]}
                  onClick={() => approveRestaurant(restaurant._id)}
                >
                  {t("admin.approve")}
                </LoadingButton>
              </div>
            </Card.Body>
          </Card>
        ))
      }
    </Container>
  );
};

export default Admin;