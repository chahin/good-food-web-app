import React, { useState, useEffect, useContext } from "react";
import { Tab, Button, Nav, OverlayTrigger, Tooltip, Col } from "react-bootstrap";
import { Link, useHistory, useLocation } from "react-router-dom";
import { Home, Orders, Menu } from ".";
import store from "../store";
import HomeRoundedIcon from "@material-ui/icons/HomeRounded";
import { ReceiptRounded, RestaurantMenuRounded } from "@material-ui/icons";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";
import { Admin } from "./Admin";
import { useTranslation } from 'react-i18next';

const Main = function () {
  const { t } = useTranslation();
  const history = useHistory();
  const location = useLocation();
  const [tab, setTab] = useState(location.pathname);

  useEffect(() => {
    setTab(location.pathname);
  }, [location]);

  const buttonStyle = {
    width: "45px",
    height: "45px"
  };

  const signOut = (e) => {
    e.preventDefault();
    store.clear();
    history.push("/signin");
  };

  const renderTooltip = function (props, text) {
    return (
      <Tooltip id="button-tooltip" {...props}>
        {text}
      </Tooltip>
    );
  }

  return <Tab.Container activeKey={tab} transition={false}>
    <div className="vh-100 bg-light d-flex justify-content-between">
      <div className="p-0 bg-white" style={{ minWidth: "80px" }}>
        <div className="d-flex flex-column align-items-center pt-3">
          <Nav variant="pills" className="flex-column">

            {
              store.isAdmin ? (
                <Nav.Item className="mb-3" style={buttonStyle}>
                  <Link className="text-decoration-none" to="/admin">
                    <OverlayTrigger
                      placement="right"
                      delay={{ show: 500, hide: 0 }}
                      overlay={(props) => renderTooltip(props, "Administration")}
                    >
                      <Nav.Link eventKey="/admin" as={Button} variant="light" className="rounded-extra p-0" style={buttonStyle}>
                        <HomeRoundedIcon />
                      </Nav.Link>
                    </OverlayTrigger>
                  </Link>
                </Nav.Item>
              ) : (
                <>
                  <Nav.Item className="mb-3" style={buttonStyle}>
                    <Link className="text-decoration-none" to="/home">
                      <OverlayTrigger
                        placement="right"
                        delay={{ show: 500, hide: 0 }}
                        overlay={(props) => renderTooltip(props, t("home.title"))}
                      >
                        <Nav.Link eventKey="/home" as={Button} variant="light" className="rounded-extra p-0" style={buttonStyle}>
                          <HomeRoundedIcon />
                        </Nav.Link>
                      </OverlayTrigger>
                    </Link>
                  </Nav.Item>
                  <hr className="m-0 mb-3 border-lighter" style={{ width: "45px" }} />
                  <Nav.Item className="mb-3">
                    <Link className="text-decoration-none" to="/orders">
                      <OverlayTrigger
                        placement="right"
                        delay={{ show: 500, hide: 0 }}
                        overlay={(props) => renderTooltip(props, t("orders.title"))}
                      >
                        <Nav.Link eventKey="/orders" as={Button} variant="light" className="rounded-extra p-0" style={buttonStyle}>
                          <ReceiptRounded />
                        </Nav.Link>
                      </OverlayTrigger>
                    </Link>
                  </Nav.Item>
                  <Nav.Item className="mb-3">
                    <Link className="text-decoration-none" to="/menu">
                      <OverlayTrigger
                        placement="right"
                        delay={{ show: 500, hide: 0 }}
                        overlay={(props) => renderTooltip(props, t("menu.title"))}
                      >
                        <Nav.Link eventKey="/menu" as={Button} variant="light" className="rounded-extra p-0" style={buttonStyle}>
                          <RestaurantMenuRounded />
                        </Nav.Link>
                      </OverlayTrigger>
                    </Link>
                  </Nav.Item>
                </>
              )
            }

            <Nav.Item className="mb-3">
              <a href="##" className="text-decoration-none" onClick={signOut}>
                <OverlayTrigger
                  placement="right"
                  delay={{ show: 500, hide: 0 }}
                  overlay={(props) => renderTooltip(props, t("signOut"))}
                >
                  <Nav.Link as={Button} variant="light" className="rounded-extra p-0" style={buttonStyle}>
                    <ExitToAppRoundedIcon />
                  </Nav.Link>
                </OverlayTrigger>
              </a>
            </Nav.Item>
          </Nav>
        </div>
      </div>
      <div className="h-100 flex-grow-1 bg-lighter">
        <Tab.Content className="h-100">
          {
            store.isAdmin ? (
              <Tab.Pane eventKey="/admin" className="h-100">
                <Admin />
              </Tab.Pane>
            ) : (
              <>
                <Tab.Pane eventKey="/home" className="h-100">
                  <Home />
                </Tab.Pane>
                <Tab.Pane eventKey="/orders" className="h-100">
                  <Orders />
                </Tab.Pane>
                <Tab.Pane eventKey="/menu" className="h-100">
                  <Menu />
                </Tab.Pane>
              </>
            )
          }
        </Tab.Content>
      </div>
    </div>
  </Tab.Container >
};

export default Main;