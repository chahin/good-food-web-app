import React, { useContext, useEffect, useRef, useState } from "react";
import { AlertContext } from "app/context/AlertContext";
import api from "app/service";
import { Card, Carousel, Col, Container, Form, Image, Row } from "react-bootstrap";
import { useLocation } from "react-router";
import LoadingButton from "app/components/LoadingButton";
import { useTranslation } from 'react-i18next';
const IMAGES_UPLOAD_KEY = "restaurant-image";

const Home = function () {
  const { t } = useTranslation();
  const showAlert = useContext(AlertContext);
  const fileInput = useRef(null);
  const location = useLocation();
  const [loading, setLoading] = useState(false);

  const [images, setImages] = useState([]);

  useEffect(() => {
    if (location.pathname === "/home") {
      getImages()
    }
  }, [location]);


  const getImages = () => {
    api.getImages({
      loading: (val) => setLoading(val),
      error: (err) => showAlert({ message: t("home.getImageError"), type: "error" }),
      success: (res) => setImages(res)
    });
  }

  const uploadImages = (images) => {
    if (images.length > 0) {
      const data = new FormData();
      for (const f of images) {
        data.append(IMAGES_UPLOAD_KEY, f);
      }

      api.uploadImages(
        data,
        {
          loading: (val) => null,
          error: (err) => showAlert({ message: t("home.uploadImageError"), type: "error" }),
          success: (res) => {
            showAlert({ message: t("home.uploadImageSuccess") });
            getImages();
          }
        }
      )
    }
  }

  return <Container fluid className="h-100 overflow-x-hidden overflow-auto p-4">
    <h4 className="font-bold m-0 py-2 mb-3">{t("home.title")}</h4>
    <Row className="ml-3 mt-4">
      <Col lg={4}>
        <Card className="bg-white shadow rounded-extra border-0">
          <Card.Body>
            <h5 className="font-semi-bold mb-4">{t("home.myImages")}</h5>
            {
              images.length > 0 ? (
                <Carousel className="rounded-extra" style={{ overflow: 'hidden' }}>
                  {images.map((image, index) => (
                    <Carousel.Item key={index}>
                      <div
                        style={{
                          height: 200,
                          backgroundImage: `url(${image || "../../assets/missing-image.png"})`,
                          backgroundPosition: "center",
                          backgroundSize: "cover"
                        }}
                      />
                    </Carousel.Item>
                  ))}
                </Carousel>
              ) : (
                <div className="d-flex align-items-center mx-3">
                  <span className="text-gray">{t("home.noImage")}</span>
                  <div>
                    <LoadingButton
                      className="ml-2"
                      size="sm"
                      loading={loading}
                      onClick={() => fileInput.current.click()}
                    >
                      {t("home.uploadHere")}
                    </LoadingButton>
                  </div>
                </div>
              )
            }
          </Card.Body>
        </Card>
      </Col>
    </Row>
    <Form.Group className="position-relative mb-3">
      <Form.Control
        ref={fileInput}
        type="file"
        required
        name="file"
        multiple
        accept="image/png, image/jpeg"
        onChange={(e) => uploadImages(e.target.files)}
      />
    </Form.Group>
  </Container>
};

export default Home;
