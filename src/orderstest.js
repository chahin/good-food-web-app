export const orders = [
    {
        _id: "1", // order number
        restaurant_id: "1",
        user_id: "1",
        date: new Date(),
        content: "Kebab Frite, salade tomate oignons, sauce algérienne",
        delivery_address: "27 rue de la canardiere",
        orders_id: "CF45HG", // Id commande
        client_name: "KALIA K.",
        prepared: "11:59",
        price: "13,50 €",
    },
    {
        _id: "2", // order number
        restaurant_id: "1",
        user_id: "1",
        date: new Date(),
        content: "Yufka, salade tomate oignons, sauce algérienne",
        delivery_address: "27 rue de la canardiere",
        orders_id: "ZFZE52", // Id commande
        client_name: "DEDH K.",
        prepared: "11:59",
        price: "10,50 €",
    },
    {
        _id: "2", // order number
        restaurant_id: "1",
        user_id: "1",
        date: new Date(),
        content: "Yufka, salade tomate oignons, sauce algérienne",
        delivery_address: "27 rue de la canardiere",
        orders_id: "ZUEH7F", // Id commande
        client_name: "FUHR K.",
        prepared: "11:59",
        price: "10,50 €",
    },
    {
        _id: "2", // order number
        restaurant_id: "1",
        user_id: "1",
        date: new Date(),
        content: "Yufka, salade tomate oignons, sauce algérienne",
        delivery_address: "27 rue de la canardiere",
        orders_id: "EF3E5G", // Id commande
        client_name: "John K.",
        prepared: "11:59",
        price: "10,50 €",
    },
];
