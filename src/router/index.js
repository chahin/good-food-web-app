import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();
export { default as ProtectedRoute } from './ProtectedRoute';
export { default as RedirectedRoute } from './RedirectedRoute';