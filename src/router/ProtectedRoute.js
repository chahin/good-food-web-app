import { NotFound } from "app/domain";
import React from "react";
import { Route, Redirect } from "react-router-dom";
import store from "../app/store";

const ProtectedRoute = ({ component: Component, isAdmin = false, ...rest }) => {
  return <Route
    {...rest}
    render={props => {
      if (!store.isLogged()) {
        return <Redirect to={{
          pathname: "/signin",
          state: {
            alert: {
              message: "mamacita",
              type: "error"
            }
          }
        }}
        />
      }

      if (isAdmin) {
        return store.isAdmin ? <Component {...props} /> : <NotFound />;
      } else {
        return store.isAdmin ? <NotFound /> : <Component {...props} />
      }
    }
    }
  />
};

export default ProtectedRoute;