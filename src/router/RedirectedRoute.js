import React from "react";
import { Route, Redirect } from "react-router-dom";
import store from "../app/store";

const RedirectedRoute = ({ component: Component, ...rest }) => {
  return <Route
    {...rest}
    render={(props) => store.isLogged()
      ? <Redirect to={{ pathname: store.isAdmin ? "/admin" : "/home" }} />
      : <Component {...props} />
    }
  />
};

export default RedirectedRoute;
