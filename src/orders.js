export const orders = [
    {
        _id: "1", // order number
        restaurant_id: "1",
        user_id: "1",
        date: new Date(),
        content: {
            order: [
                {
                    Plat: [
                        {
                            Sandwich: "yufka",
                            Crudites: ["salade", "tomate", "oignon"],
                            Sauce: "Algerienne",
                        },
                        {
                            Sandwich: "yufka",
                            Crudites: ["salade", "tomate", "oignon"],
                            Sauce: ["Algerienne", "biggy"],
                        },
                    ],
                    Boisson: "Coca-Cola",
                    Dessert: ["Tiramisu", "Flan"],
                    Entrer: "salade cesar",
                },
            ],
        },
        delivery_address: "16 rue de milan",
        price: "10,50 $",
        prepared: "11:59",
        orders_id: "BD15G",
        client_name: "John K.",
    },
    {
        _id: "2", // order number
        restaurant_id: "1",
        user_id: "1",
        date: new Date(),
        content: {
            Plat: {
                nom: "yufka",
                crudites: ["salade", "tomate", "oignon"],
                sauce: "sauce algerienne",
            },
            Boisson: "Coca-Cola",
            Dessert: ["Tiramisu", "Flan"],
            Entrer: "salade cesar",
        },
        delivery_address: "27 rue de la canardiere",
        price: "10,50 $",
        prepared: "11:58",
        orders_id: "AG13H",
        client_name: "Marie F.",
    },
];
