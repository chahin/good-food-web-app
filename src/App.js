// @ts-nocheck
import React, { useEffect } from "react";
import { Router, Switch, Route, Redirect } from "react-router-dom";
import { ProtectedRoute, RedirectedRoute } from "./router";
import { SignIn, SignUp } from "./app/domain/Authentication";
import { Main, NotFound } from "./app/domain";
import { AlertProvider, WsProvider } from "app/context";
import { history } from "./router";
import store from "./app/store";
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { EN_TRANSLATIONS } from "./translations/en.js";
import { FR_TRANSLATIONS } from "./translations/fr.js";
import LanguageDetector from 'i18next-browser-languagedetector';

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    detection: {
      order: ["navigator"]
    },
    resources: {
      en: { translation: EN_TRANSLATIONS }, fr: { translation: FR_TRANSLATIONS }
    },
    fallbackLng: 'fr',
    debug: process.env.NODE_ENV === "development"
  });

i18n.changeLanguage("fr")


function App() {
  return (
    <Router history={history}>
      <AlertProvider>
        <WsProvider>
          <Switch>
            <Route exact path="/">
              {
                store.isLogged()
                  ? <Redirect to={`${store.isAdmin ? "/admin" : "/home"}`} />
                  : <Redirect to='/signin' />
              }
            </Route>

            <RedirectedRoute path="/signin" component={SignIn} />
            <RedirectedRoute path="/signup" component={SignUp} />
            <ProtectedRoute path={["/home", "/orders", "/menu"]} component={Main} />
            <ProtectedRoute path="/admin" isAdmin component={Main} />
            <Route path="*" component={NotFound} />
          </Switch>
        </WsProvider>
      </AlertProvider>
    </Router>
  );
}

export default App;
