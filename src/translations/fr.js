export const FR_TRANSLATIONS = {
  categories: {
    "African": "Africain",
    "Asian": "Asiatique",
    "Chinese": "Chinois",
    "Fast-Food": "Fast-Food",
    "Indian": "Indien",
    "Italian": "Italien",
    "Japanese": "Japonais",
    "Korean": "Coréen",
    "Turkish": "Turc"
  },
  signIn: {
    title: "Se Connecter",
    email: "Email",
    password: "Mot de passe",
    submit: "Valider",
    noAccount: "Vous n'avez pas de compte? S'inscrire",
    invalidEmail: "Email invalide",
    required: "Required",
  },
  signUp: {
    title: "S'inscrire",
    step: "Étape",
    email: "Email",
    password: "Mot de passe",
    confirmPassword: "Confirmation",
    restaurantName: "Nom du restaurant",
    category: "Catégorie",
    iban: "IBAN",
    bic: "BIC",
    next: "Suivant",
    importantDocs: "Documents importants",
    choose: "Choisir...",
    importId: "Importer ID",
    importSiret: "Importer SIRET",
    alreadyAccount: "Déjà inscrit? Se connecter",
    required: "requis",
    invalidEmail: "Email invalide",
    passwordMustMatch: "Mots de passe différents",
    hours: "Horaires",
    address: "Adresse",
    previous: "Précédent",
    submit: "Valider",
    wrongAddress: "Adresse incorrecte",
    wrongLoginPassword: "Email ou mot de passe incorrecte"
  },
  home: {
    title: "Accueil",
    myImages: "Mes images",
    noImage: "Vous n'avez aucune image.",
    uploadHere: "Importer ici",
    getImageError: "Une erreur est survenue lors de la récupération des images.",
    uploadImageError: "Une erreur est survenue lors de la sauvegarde des images.",
    uploadImageSuccess: "Images sauvegardées avec succès."
  },
  menu: {
    title: "Menu",
    placeholder: "Nom de la section: ex 'Sandwichs'",
    renameSection: "Renommer la section",
    deleteSection: "Supprimer la section",
    add: "Ajouter",
    example: "Titre de section: ex 'Sandwichs'",
    addSection: "Ajouter une section",
    addElementIn: "Ajouter un élement dans",
    description: "Description",
    content: "Contenu",
    name: "Nom",
    initialPrice: "Prix initial",
    icon: "Icône",
    available: "Disponible",
    unavailable: "Indisponible",
    addImage: "Ajouter image",
    cancel: "Annuler",
    next: "Suivant",
    submit: "Valider",
    extras: "Suppléments",
    numberOfChoices: "Nombre de choix possibles",
    min: "Min",
    max: "Max",
    choices: "Choix",
    choiceDesc: "Décocher le switch pour rendre une option indisponible",
    addPrice: "Ajouter price",
    price: "Prix",
    delete: "Supprimer",
    edit: "Modifier",
    required: "Requis",
    options: "Options",
    noImage: "Pas d'image",
    yes: "Oui",
    menuSaved: "Menu sauvegardé",
    couldNotSaveMenu: "Impossible de sauvegarder le menu",
    nothingYet: "Vide",
    previous: "Précédent",
    optional: "Optionnel",
    modifierPlaceholder: "Ex: Suppléments",
    save: "Sauvegarder",
    cancelEdit: "Annuler l'édition",
    optionName: "Nom de l'option",
  },
  orders: {
    title: "Commandes",
    new: "Nouvelles",
    inPreparation: "En preparation",
    sent: "Envoyées",
    done: "Terminée",
    invoice: "Facture",
    refuse: "Refuser",
    accept: "Accepter",
    empty: "Vide",
    reason: "Motif",
    refuseOrder: "Refus de la commande",
    clarifyReason: "Veuillez préciser le motif du refus",
    comment: "Note"
  },
  admin: {
    applications: "Demandes de restaurants",
    reject: "Rejeter",
    approve: "Approver"
  },
  signOut: "Se déconnecter",
  cancel: "Annuler",
  yes: "Oui",
  areYouSure: "Êtes-vous sûr(e)?",
  changesMayNotBeSaved: "Changes you made may not be saved.",
}