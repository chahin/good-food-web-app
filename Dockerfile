FROM node:latest

WORKDIR /app
COPY ./package.json /app/
COPY ./yarn.lock /app/
RUN yarn --ignore-optional

COPY . /app

CMD ["yarn", "build"]